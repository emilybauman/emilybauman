
## About me 👋
 
 - My name is Emily Bauman and I am a [Senior Product Designer](https://about.gitlab.com/handbook/engineering/ux/product-design/) supporting the [ModelOps](https://about.gitlab.com/direction/modelops/) and [AI-Powered](https://about.gitlab.com/direction/ai-powered/) Stages. This means I support AI teams as they need me, and am not dedicated to a specific team! 
      - My current focus is improving the UX of [Duo Chat](https://docs.gitlab.com/ee/user/gitlab_duo_chat/) and [GitLab Duo with Amazon Q](https://docs.gitlab.com/ee/user/duo_amazon_q/). 
 - Previously I worked with the Environments Team, Release Team and the Growth Activation, Conversion and Expansion Teams. 

## My working style
 - I work in [Eastern Standard Time / Eastern Daylight Time](https://time.is/Toronto) and I'm usually online somewhere between 8:00am to 8:30am. 
 - I keep track of all my Weekly Tasks in this [folder](https://gitlab.com/emilybauman/about-me/-/tree/master/Weekly%20Tasks).
 - I try to keep all meetings focused in chunks in my mornings, and leave my afternoons for deep work. Late meetings put me in _waiting mode_ all day. 
 - I'm always open for a Coffee Chat. ☕

## History as a Product Designer
 - I started off my creative career as a part time SFX and Makeup Artist for events while I was finishing my degree. 
 - I began my Product Design journey at IBM, working on their Internal Website and later Supply Chain Software. 
 - I worked at Capital One before GitLab, with a focus on Customer Growth and Acquisitions. 

## A little about me
 
 - I live in Toronto, Ontario, Canada. 🍁 
 - My personality type is [ENFJ-T](https://www.16personalities.com/enfj-personality) - The Protagonist.
 - I am a cat mom to two blind kitties named Harriet and Claude. 
 - My main hobbies include travelling, skiing, camping and anything else outdoor related. 

<details><summary>I like travelling, so here is a little list of Countries and Territories I've been to:</summary>
 🇦🇩 🇦🇹 🇧🇸 🇧🇪 🇧🇿 🇧🇲 🇧🇼 🇨🇦 🇨🇳 🇨🇴 🇨🇷 🇨🇺 🇨🇿 🇩🇰 🇩🇴 🇸🇻 🇪🇨 🇪🇬 🇪🇪 🇪🇹 🇫🇮 🇫🇷 🇩🇪 🇬🇮 🇬🇹 🇭🇳 🇭🇺 🇮🇸 🇮🇪 🇮🇹 🇯🇵 🇯🇴 🇰🇪 🇱🇦 🇱🇮 🇱🇺 🇲🇼 🇲🇽 🇲🇨 🇳🇦 🇳🇱 🇳🇮 🇳🇴 🇵🇦 🇵🇹 🇱🇨 🇸🇮 🇿🇦 🇪🇸 🇸🇪 🇨🇭 🇹🇿 🇹🇭 🇹🇷 🇦🇪 🇬🇧 🇺🇸 🇿🇲 🇿🇼 
</details>
