## Week of August 29th, 2022 to September 2nd, 2022

## 15.4 Priorities
- [15.4 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/153)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Emily / Andy Interview training prep
 - [x] Interview Hold
 - [x] Release UX/PM sync
 - [x] Toronto GitLab Lunch Meetup
 - [x] Rayana & Emily 1:1
 - [x] Release Stage Brainstorm session
 - [x] Release UX/FE sync
 - [ ] Chris / Will / Rayana / Emily - Monthly Check-in
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Record video for [UX Roadmap for Release](https://gitlab.com/gitlab-org/gitlab/-/issues/364527).
 - [x] Clean up issue for [Support multiple approval rules in project-level protected environments settings UI](https://gitlab.com/gitlab-org/gitlab/-/issues/367700).
 - [x] Start on Prototype and questions for [Solution Validation: Validate how users want to be notified about pending approvals](https://gitlab.com/gitlab-org/ux-research/-/issues/2070).
 - [x] Clean up issue for [Support Approval Rules in Deployment Approval UI](https://gitlab.com/gitlab-org/gitlab/-/issues/355708).
 - [x] Clean up issue for [Add form validation when required approvers exceeds number of users allowed to approve](https://gitlab.com/gitlab-org/gitlab/-/issues/356228).

### Other Tasks:
 - [x] Complete first shadow opportunity for [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1864).
 - [x] Record a PTO Walkthrough of Open issues. 
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
