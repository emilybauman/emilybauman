## Week of November 29, 2021 to December 3rd, 2021

## 14.6 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/504)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/505) this milestone.

### Key Meetings:
 - [x] Jacki's Team Meeting
 - [x] UX Weekly (Cancelled)
 - [x] 1:1 with Nick
 - [x] 1:1 with Jacki
 - [x] Catchup with Gayle
 - [x] OKR Catchup with Nick Post
 - [x] 1:1 with Matej
 - [x] Aquisition Team Sync
 - [x] Growth Conversion Team Meeting
 - [x] Q4 Expansion Check-in
 - [x] 1:1 with Jay
 - [x] Async with Kevin
 - [x] Expansion Mid-Milestone Catchup (Cancelled)
 - [x] Design Pair with Andy

### Tasks for Activation:
 - [x] Review what work was completed over the past two weeks while I was out of office.
 - [x] Start work on [Add confirm identity via phone number to validation in first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/501)
 - [ ] Take a look at [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465)

### Tasks for Expansion:
 - [x] Review what work was completed over the past two weeks while I was out of office.
 - [x] Start work on [Add high conversion trial actions to Continuous onboarding](https://gitlab.com/gitlab-org/gitlab/-/issues/345174)
 - [ ] Take a look at [Experiment UX: during cart abandonment offer trial and talk to sales option
](https://gitlab.com/gitlab-org/gitlab/-/issues/343179)

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [ ] Look into heuristic buddy tasks on [Manage:Optimize FY22-Q4 - Evaluating VSA](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12593)

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
