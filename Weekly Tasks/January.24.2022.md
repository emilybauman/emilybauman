## Week of January 24th, 2022 to January 28th, 2022

## 14.8 Priorities
- [Activation Planning Issue TBD]()
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/547) this milestone.

### Key Meetings:
 - [x] Free User Strategy Product/UX Sync
 - [x] 1:1 with Valerie
 - [x] Jacki's UX Team Meeting
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] Growth Sub-department Engineering Weekly
 - [x] 1:1 with Matej
 - [x] Aquisitions Team Sync
 - [x] Growth Conversion Team Meeting
 - [x] Async with Kevin
 - [x] Growth Design Sync
 - [x] 1:1 with Jacki
 - [x] Design Pair with Andy
 - [x] 1:1 with Nick

### Tasks for Activation:
 - [x] Finish work on [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465).
 - [x] Sync with Jensen on whats next

### Tasks for Expansion:
 - [x] Start on [Update /billing page to mirror /pricing page design for Free users](https://gitlab.com/gitlab-org/gitlab/-/issues/346272)
 - [x] Work on [Add Upgrade item to top-right (user profile) drop down for Trial users](https://gitlab.com/gitlab-org/gitlab/-/issues/350168)

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
