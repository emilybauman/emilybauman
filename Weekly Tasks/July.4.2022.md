## Week of July 4th, 2022 to July 8th, 2022

I am out of office on vacation in Dominican Republic. 🌴

- Please contact my manager, [Rayana](https://gitlab.com/rayana) for anything urgent.
- I will be back on **July 12th, 2022**.


