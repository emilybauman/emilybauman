## Week of January 31st, 2022 to February 4th, 2022

## 14.8 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/553)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/547) this milestone.

### Key Meetings:
 - [x] Free User Strategy Product/UX Sync
 - [x] UX Weekly
 - [x] Growth Weekly
 - [x] Async with Matej
 - [x] Aquisitions Team Sync
 - [x] UX Showcase
 - [ ] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [x] 1:1 with Jacki
 - [ ] Design Pair with Andy

### Tasks for Activation:
 - [ ] Finish design for [Allow users to back out of group creation requiring credit card validation](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/559)
 - [x] Look at what's next in [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93)

### Tasks for Expansion:
 - [ ] Finish [Update /billing page to mirror /pricing page design for Free users](https://gitlab.com/gitlab-org/gitlab/-/issues/346272)
 - [ ] Restart work on [Indicate how many seats are remaining when namespace approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/348694)

### Other Tasks:
 - [x] Work on Documentation [Popover > Document close button usage](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1225)
 - [ ] Start conversation with Foundations about [Display Invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/351020)
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
