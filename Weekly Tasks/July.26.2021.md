## Week of July 26th to July 30th, 2021

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Jacki's EFG Meeting
 - [x] Growth Weekly
 - [x] UX Weekly
 - [ ] Growth Engineering Weekly
 - [x] Acquisitions Team Sync
 - [x] Async 1:1 with Kevin
 - [x] 1:1 with Matej
 - [x] Review UX Strategy

### Tasks for Activation:
 - [ ] Finish up writing up research issue around **Project Orchestration idea**.
 - [x] Start collaboration with Nadia on [Add guided walkthrough to pipeline editor](https://gitlab.com/gitlab-org/gitlab/-/issues/334659).
 - [ ] Continue to clean up the [Experience Recommendations](https://gitlab.com/gitlab-org/growth/product/-/issues/1691) for the Verify Onboarding Scorecard.

### Tasks for Expansion:
 - [ ] Finish up Modal for [Provide admins with a sharable invite signup URL issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336240/).
 - [x] Start looking into [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/336248).

### Other Tasks:
 - [x] Schedule Coffee Chat with **Gayle Doud**
 - [x] Schedule Coffee Chat with **Nick Hertz**
 - [ ] Review [PQL Strategy](https://gitlab.com/gitlab-org/gitlab/-/issues/326828#note_622441182).
 - [x] Watch UX Showcase from previous week ([Video 1](https://www.youtube.com/watch?v=J-USG9BKH-g&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=4), [Video 2](https://www.youtube.com/watch?v=F8S7nPz2EaI&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=3), [Video 3](https://www.youtube.com/watch?v=cPQiPkmRYrw&list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz&index=2))

### Personal Goals:
 - [x] Urdu Lessons.
 - [x] Start writing up year end goals in relation to the [skills matrix](https://docs.google.com/spreadsheets/d/1tgA8SqVitI6MLFLWaCHtFJUa6x7CuuMiEwfEGAQfNyk/edit?usp=sharing).
 
### Issue Backlog:

**To Be Done**
- [Allow non-admins to request access to a feature/tier or recommend an invite](https://gitlab.com/gitlab-org/ux-research/-/issues/1297).
- [Problem validation: Invited user onboarding](https://gitlab.com/gitlab-org/ux-research/-/issues/1009).

**Designs complete, waiting on implementation**
 - [Switch Easy-Button Icons with Radio Buttons](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/400).
- [Link CI Onboarding MR Widget to the Pipeline Editor](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/399).
- [Allow users to invite others onto GitLab to help complete a task](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/393).
- [Add "welcome" questions to namespace creation UX](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/392).
- [Ask admin (inviter) what areas they'd like the invitee to focus on](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Notify inviter if invite email hard bounces](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
