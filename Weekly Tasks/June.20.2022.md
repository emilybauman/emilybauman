## Week of June 20th, 2022 to June 24th, 2022

- 🌴 Short Week - Family & Friends Day on June 24th

## 15.2 Priorities
- Focus this Milestone on Transitioning into Release and Beautifying our UI
- [Career Mobility Emily Bauman, per 2022-06-13 as Product Designer](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4157)
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] #donut-be-strangers Donut
 - [x] Coffee Chat ☕ Veethika/Emily
 - [x] Coffee Chat ☕ Russell/Emily
 - [x] Coffee Chat ☕ Bala/Emily
 - [x] Release Group Weekly [REC]
 - [x] Release, Configure, Monitor Field Sync
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Will ☕ Emily - Coffee Chat and Research Initiatives
 - [x] UX Showcase
 - [x] Coffee Chat ☕ Andrew/Emily
 - [x] Coffee Chat ☕ Gina/Emily
 - [x] Valerie/Emily - 1:1
 - [x] Release Stage Brainstorm session
 - [x] Verify Stage Think Big - Next Generation CI
 - [x] Coffee Chat ☕ Ali/Emily
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).

### Tasks for Growth:
 - [x] Meet up with Sam and Gayle to close off and transition any final growth issues.

### Other Tasks:
 - [ ] Complete 3 MRs for [Beautifying Our UI 15.2](https://gitlab.com/gitlab-org/gitlab/-/issues/362122).
 
### Personal Goals:
 - [ ] Fix up [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
 - [x] Finish my self assessment for the mid year review.
