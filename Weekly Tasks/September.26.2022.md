## Week of September 26th, 2022 to September 30th, 2022

🌴 Short week - Family and Friends Day Monday the 26th, Sick Day Tuesday the 27th.

## 15.5 Priorities
- [15.5 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/158)

### Key Meetings:
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] 1:1 Emily B and Jacki
 - [x] UX Buddy session: Emily & Gina
 - [x] Rayana & Emily 1:1
 - [x] #random-coffees-ux-dept Donut
 - [x] Release Stage Brainstorm session
 - [x] Michael / Emily - Design Pair
 - [x] Will/Emily Research Sync
 - [x] Notifications Design Jam - Sync Session 3
 
### Tasks for Release:
 - [x] Start on [Improve Empty State Pages for Deployments](https://gitlab.com/gitlab-org/gitlab/-/issues/371392).
 - [x] Continue work on [Solution Validation: Validate users are okay being notified of Pending Deployment Approvals with To-Dos](https://gitlab.com/gitlab-org/ux-research/-/issues/2070).
 - [ ] If time permits, take a look at [Don't automatically run a deployment job post approval when using Unified Approval Setting](https://gitlab.com/gitlab-org/gitlab/-/issues/367943).

### Other Tasks:
 - [x] Switch over to new computer. 
 - [ ] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [ ] If time permits, help Eugie with the [Arkose Proposal](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/96). 

 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
