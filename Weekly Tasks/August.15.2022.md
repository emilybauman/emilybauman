## Week of August 15th, 2022 to August 18th, 2022

## 15.3 Priorities
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Record Kickoff Video
 - [x] Release Group Weekly [REC]
 - [x] Release, Configure, Monitor Field Sync
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] UX Showcase
 - [x] Rayana & Emily 1:1
 - [x] Release Stage Community Office Hours [HOLD]
 - [x] Focus Group UX - Women ICs
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [ ] Get [Task 2: Prepare 1-2 JTBDs for the Release Orchestration (RO) category](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/108916) merged.
 - [ ] Finish filling out Theme Issues for [UX Roadmap for Release](https://gitlab.com/gitlab-org/gitlab/-/issues/364527).
 - [x] Finish off UX work for [Create annotated tags with Releases UI](https://gitlab.com/gitlab-org/gitlab/-/issues/362481).
 - [x] Make progress on [Support multiple approval rules in project-level protected environments settings UI](https://gitlab.com/gitlab-org/gitlab/-/issues/367700).
 - [ ] Finish filling our research proposal for [Solution Validation: Validate how users want to be notified about pending approvals](https://gitlab.com/gitlab-org/ux-research/-/issues/2070).
 - [x] If time permits, start work on [Support Approval Rules in Deployment Approval UI](https://gitlab.com/gitlab-org/gitlab/-/issues/355708) and [Show multiple deployment approval comments when multiple approval rules are configured](https://gitlab.com/gitlab-org/gitlab/-/issues/368640).


### Other Tasks:
 - [ ] Finish a proposal for [Custom Text Updates to Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/370424). 
 - [ ] Keep an eye out for shadowing opportunities based on [Interview Training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1864).
 - [x] Organize calendar and meetings for my PTO in September. 
 - [x] Provide 360 Feedback.
 - [ ] Work on [Document Verification Interactions/Methods](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1402).
 - [x] Share out the [Release Stage Community Office Hours](https://www.meetup.com/gitlab-virtual-meetups/events/287756492/).
 - [x] Expense July Internet Bill.
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
