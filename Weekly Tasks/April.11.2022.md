## Week of April 11th, 2022 to April 15th, 2022

🌴 Short week with **Family and Friends Day** on April 11th and **Good Friday** on April 15th.

## 14.10 Priorities
- [Activation/Adoption Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/619)
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/618)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/621)

### Key Meetings:
 - [x] Coffee Chat with Sam
 - [x] Growth Weekly
 - [ ] ~~UX Weekly Call~~
 - [x] Coffee Chat with Serhii
 - [x] Async with Matej
 - [x] 1:1 with Gayle and Sam
 - [x] Activation Team Sync
 - [x] UX Showcase
 - [x] Growth Conversion Team Meeting
 - [x] 1:1 with Kevin
 - [ ] ~~1:1 with Jacki~~
 - [x] Test & Learn 1: Test Preparation
 - [x] 1:1 with Jensen

### Tasks for Activation:
 - [ ] Record video for [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).
 - [ ] Finish off [Add walkthrough/wizard for the standard CI template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/572).
 - [ ] Plan for next Milestone

### Tasks for Expansion:
 - [x] Finish off [UX: Enable Invite modal to handle multiple errors at once](https://gitlab.com/gitlab-org/gitlab/-/issues/352917)
 - [x] If time permits, work on the following: [UX: Personal (user) namespace: Indicate how many seats are remaining when namespace approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/358622), [UX: Personal (user) namespace: Lock invite options when namespace reaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/358624), [UX: Personal (user) namespace - When namespace reaches user limit alert Owners via banners and email](https://gitlab.com/gitlab-org/gitlab/-/issues/358625) and [UX: Personal (user) namespace - When namespace reaches user limit alert Owners via banners and email](https://gitlab.com/gitlab-org/gitlab/-/issues/358625).

### Other Tasks:
 - [ ] Finish Week 3 of Reforge Content.

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
