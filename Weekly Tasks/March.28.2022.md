## Week of March 28th, 2022 to April 1st, 2022

## 14.10 Priorities
- [Activation/Adoption Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/619)
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/618)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/621)

### Key Meetings:
 - [x] Jacki's UX Team (FG) Meetup
 - [x] UX Weekly Call
 - [x] Metaphysics Coffee Chat
 - [x] Growth Weekly
 - [x] Emily / Gayle/Sam Bi-Weekly
 - [x] Activation Team Sync
 - [x] Growth Conversion Team Meeting
 - [ ] 1:1 Emily B and Jacki
 - [ ] Jensen/Emily Sync

### Tasks for Activation:
 - [x] Continue work on [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).
 - [ ] When able to, continue work on [Add walkthrough/wizard for the standard CI template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/572).

### Tasks for Expansion:
 - [x] Finish off [On members page add a link to the usage quota seats page](https://gitlab.com/gitlab-org/gitlab/-/issues/353226).
 - [x] Get started on [UX: Ability for admin to view and control who can access their namespace - active trial](https://gitlab.com/gitlab-org/gitlab/-/issues/354536).
 - [ ] If time permits, also look at [UX: Trial start: Inform owner they can add as many users as they want in the trial period; note return to X users and free plan at end.](https://gitlab.com/gitlab-org/gitlab/-/issues/347440).

### Other Tasks:
 - [ ] Finish Week 1 of Reforge Content.

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
