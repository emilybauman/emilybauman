## Week of December 19th, 2022 to December 23rd, 2022

🌲 Slightly Short Week: OOO December 23rd at 12:00pm EST

## 15.8 Priorities
- [15.8 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/202)

### Key Meetings:
 - [x] Gitlab Release Discussion
 - [x] Release Group Weekly [REC]
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Emily & Gina sync
 - [x] Release UX/PM sync
 - [x] Release Stage Brainstorm session [REC]

### Tasks for Release:
 - [x] Finish design for [Link to existing Environments from Project Home](https://gitlab.com/gitlab-org/gitlab/-/issues/349757).
 - [ ] Finish design for [Provide a way to easily navigate to the Protected Environments setting through the Environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/378926).
 - [x] Get started on [Make navigating to the individual environment page more obvious](https://gitlab.com/gitlab-org/gitlab/-/issues/375610).
 - [x] Help Ahmed with any tasks needed for the UX sandbox with [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2121).
 - [ ] Launch first version of study for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).

### Other Tasks:
 - [x] Finalize coverage issue for December 23rd - January 2nd.
 - [x] Finalize Document for Rayana. 
 - [x] Prep for PTO. 
 - [x] Source-o-thon 
 
### Personal Goals:
 - [x] Start thiking about FY24 Goals. 
