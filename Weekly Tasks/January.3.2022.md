## Week of January 3rd, 2022 to January 7th, 2022

Short Week - Off January 3rd and 4th.

## 14.7 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/530)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/528) this milestone.

### Key Meetings:
 - [x] Aquisition Team Sync
 - [x] Conversion UX Team Sync
 - [x] 1:1 with Jacki
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [x] 1:1 with Gayle
 - [x] Growth UXR Connect

### Tasks for Activation:
 - [x] Catch up on anything I missed while away. 
 - [ ] Start work on [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465)

### Tasks for Expansion:
 - [x] Catch up on anything I missed while away. 
 - [x] Finish up work on [When namespace reaches user limit alert Owners via banners and email](https://gitlab.com/gitlab-org/gitlab/-/issues/348696)
 - [ ] Start on [Indicate how many seats are remaining when namespace approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/348694)

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
