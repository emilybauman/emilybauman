## Week of September 13th to September 15th, 2021

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Coffee Chat with Nima
 - [x] Coffee Chat with Michael
 - [x] UX Weekly
 - [x] Coffee Chat with Dorcas
 - [x] Growth Weekly
 - [x] Coffee Chat with Roy
 - [x] Aquisitions Team Sync
 - [x] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [x] Expansion Milestone Kickoff
 - [x] Growth & Fulfillment Skip-Level
 - [x] Design Pair with Andy
 
### Tasks for Activation:
 - [x] Get started on [Pitch a paid plan when users click to purchase CI minutes](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/420).
 - [ ] Work on draft screener survey and problem validation issue for [Project Orchestration idea](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/418).
 - [x] Add in content around gifts for completing an interview to [Update survey response Calendly URL](https://gitlab.com/gitlab-org/gitlab/-/issues/333977).
 - [x] Finalize any outstanding work for [Allow users to invite others onto GitLab to help complete a task](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/393).

### Tasks for Expansion:
 - [ ] Work with Nick on the synthesis of [Allow non-admins to request access to a feature/tier or recommend an invite](https://gitlab.com/gitlab-org/ux-research/-/issues/1297).
 - [x] Prepare to launch survey for [Problem validation: Invited user onboarding](https://gitlab.com/gitlab-org/ux-research/-/issues/1009).
 - [x] Put finishing touches on [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/336248).

### Other Tasks:
 - [ ] Ongoing work around [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [ ] Make [Growth L&D Recommendations](https://gitlab.com/gitlab-org/growth/ui-ux/-/issues/102) reccomendations when I find things. 

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] Urdu Lessons
 
### Issue Backlog:

**To Be Done**
- [Experiment - Add expiration copy to invited user email](https://gitlab.com/gitlab-org/gitlab/-/issues/337860)
- [Experiment - Notify non-admin users to contact admin to invite new users to namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/337555)

**Designs complete, waiting on implementation**
- [Provide admins with a sharable invite signup URL issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336240/)
- [Add "welcome" questions to namespace creation UX](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/392).
- [Switch Easy-Button Icons with Radio Buttons](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/400).
- [Link CI Onboarding MR Widget to the Pipeline Editor](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/399).
- [Ask admin (inviter) what areas they'd like the invitee to focus on](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Notify inviter if invite email hard bounces](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Net Improvement - Fix Invite modal copy to accurately communicate "access expires" impact](https://gitlab.com/gitlab-org/gitlab/-/issues/337865). 
- [Add guided walkthrough to pipeline editor](https://gitlab.com/gitlab-org/gitlab/-/issues/334659)
