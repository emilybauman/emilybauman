## Week of October 11th to October 15th, 2021

Short Week - Canadian Thanksgiving, Family and Friends Day 

## 14.4 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/439)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/437)

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] Aquisitions Team Sync
 - [x] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] GitLab Livestream
 - [x] Retention and Engagement - Growth Series
 - [ ] Andy and Emily Design Pair

### Tasks for Activation:
 - [ ] Gather feedback and finalize design for [Make user invitation (with invite for help) it's own dedicated step in first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/441).
 - [x] Get first draft design done for [Add user invitation track to day zero for "company" namespaces](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/444)
 - [ ] Pick up another issue if capacity allows

### Tasks for Expansion:
 - [ ] Start working on discussion guide for [Problem Validation: How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755)
 - [x] Work with Gayle and Kati to finalize request issue for [Experiment - Notify non-admin users to contact admin to invite new users to namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/337555)
 - [x] Finish up design for [Improve invite modal error alerts when member was already invited](https://gitlab.com/gitlab-org/gitlab/-/issues/334280)

### Other Tasks:
 - [ ] Continue work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [x] Finish up Reforge lessons for the week - Retention and Engagement - Growth Series
 - [ ] Start next weeks classes on Reforge

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] Urdu Lessons
