## Week of December 5th, 2022 to December 9th, 2022

## 15.7 Priorities
- [15.7 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/174)

❤️‍🩹 Short week - Unexpected time off Monday, December 5th

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Release UX/FE sync
 - [x] Release UX/PM sync
 - [x] Michael / Emily - Design Pair
 - [x] Emily / Kevin - Sync
 - [x] Option 1: Post-Earnings GitLab Assembly
 - [x] Emily & Gina sync

### Tasks for Release:
 - [ ] Finalize research proposal with one final round of feedback for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [ ] Create next iteration for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [x] Do one final round of reviews on [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526) and get started on the UX Sandbox.
 - [ ] Get first iteration done for [Allow users to promote a deployment to it's own environment on the Environment Index Page
](https://gitlab.com/gitlab-org/gitlab/-/issues/378552).

### Other Tasks:
 - [x] Finish next draft of Document for Rayana. 
 - [x] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [x] Finish nomination. 
 - [x] Finish 15.8 plan. 
 - [x] Complete Dovetail Survey

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
