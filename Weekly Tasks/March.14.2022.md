## Week of March 14th, 2022 to March 18th, 2022

## 14.9 Priorities
- [Activation/Adoption Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/569)
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/568)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/570)

### Key Meetings:
 - [x] Jacki's UX Team Meeting
 - [x] UX Weekly
 - [x] Growth Weekly
 - [x] 1:1 with Matej
 - [x] Activation Team Meeting
 - [x] Conversion 14.10 Kickoff
 - [x] Emily/Gayle/Sam Sync
 - [x] UX Showcase
 - [x] Growth and Fulfillment Skip-Level
 - [x] 1:1 with Jacki
 - [x] 1:1 with Jensen

### Tasks for Activation:
 - [x] Finish off the Pipeline Walkthrough section of [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).
 - [ ] Get started on [Add walkthrough/wizard for the standard CI template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/572).
 - [x] Plan for next milestone.

### Tasks for Expansion:
 - [x] Finish off work on [Wireframes for UXR - Ability for admin to view and control who can access their namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/347438).
 - [x] Plan for next milestone.

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
