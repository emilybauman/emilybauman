## Week of February 14th, 2022 to February 18th, 2022

## 14.8 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/553)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/547) this milestone.

### Key Meetings:
 - [x] Skip Level with Christie
 - [x] GitLab Assembly FY23
 - [x] Growth Weekly
 - [x] Async with Matej
 - [x] Aquisitions Team Sync
 - [x] Conversion 14.9 Kickoff
 - [x] 1:1 with Jensen
 - [x] 1:1 with Gayle
 - [x] 1:1 with Kevin
 - [x] 1:1 with Jacki

### Tasks for Activation:
 - [x] Finish and close off any quiestions around [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465).
 - [ ] Finish up [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).

### Tasks for Expansion:
 - [x] Close off [Update /billing page to mirror /pricing page design for Free users](https://gitlab.com/gitlab-org/gitlab/-/issues/346272).
 - [x] Close off [Conversion UX: Create end of trial splash page](https://gitlab.com/gitlab-org/gitlab/-/issues/350167).
 - [ ] If time allows, work on [Wireframes for UXR - Ability for admin to view and control who can access their namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/347438).

### Other Tasks:
 - [x] Close off [Add New Popover with Close Button Variant to Pajamas](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1233).
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
