## Week of August 1st, 2022 to August 5th, 2022

🌴 Short week, Monday August 1st is Civic Holiday. 

## 15.3 Priorities
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] AMA – Shopify's Philip Müller about Domains, Events, and Friends
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] UX Showcase
 - [x] Rayana & Emily 1:1
 - [x] Emily / Kevin - Sync
 - [x] Release Stage Brainstorm session
 - [x] Designer pair: Becka/ Emily
 - [ ] #random-coffees-ux-dept Donut
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [ ] Finish the JTBD and Research issue for [Task 2: Prepare 1-2 JTBDs for the Release Orchestration (RO) category.](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] [Review issues for 15.4](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/153#note_1042741615).  
 - [ ] Finish up the [UX Roadmap for Release](https://gitlab.com/gitlab-org/gitlab/-/issues/364527).
 - [x] Clean up some existing UX Issues. 

### Other Tasks:
 - [x] Finish up work on [redesigning the login page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91673). 
 - [x] Finish up the [UX Showcase presentation](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2045). 
 - [ ] Continue working through [interview training tasks and readings](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1864). 
 - [x] Finish the Managing So Everyone Can Contribute (MECC) Training. 
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
