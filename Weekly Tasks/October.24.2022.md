## Week of October 24th, 2022 to October 28th, 2022

🌴 Async Week - Visiting Grant in Medellin, Colombia. 

## 15.6 Priorities
- [15.6 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/162)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] Release Stage Community Office Hours
 - [x] Rayana & Emily 1:1
 - [x] Async - Release UX/PM sync
 - [x] Release UX/FE sync

### Tasks for Release:
 - [x] Get a first iteration done for [Deployment Detail Page MVC - Deploy Approval](https://gitlab.com/gitlab-org/gitlab/-/issues/374538).
 - [ ] Discuss next steps for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [ ] Finish up Scenarios for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [ ] Close off work for [Provide a mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/-/issues/19724).
 - [x] If time allows, start on [Don't allow a deployment to be rerun after it's been approved](https://gitlab.com/gitlab-org/gitlab/-/issues/375512).

### Other Tasks:
 - [x] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
