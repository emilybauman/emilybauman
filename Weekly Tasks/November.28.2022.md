## Week of November 28th, 2022 to December 2nd, 2022

## 15.7 Priorities
- [15.7 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/174)

### Key Meetings:
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Will/Emily Research Sync
 - [x] Release Group Weekly [REC]
 - [x] Discuss CI/CD permissions with Daniel
 - [x] Catch up – Emily & Phil
 - [ ] UX Weekly Call
 - [ ] Release UX/PM sync
 - [ ] Release Stage Community Office Hours [REC]
 - [ ] Rayana & Emily 1:1
 - [ ] Release UX/FE sync
 - [ ] CI/CD UX Meeting (EMEA/Americas)
 - [ ] Emily / Kevin - Sync
 - [ ] Emily & Gina sync
 - [ ] Emily / Vladimir - coffee chat
 - [ ] Michael / Emily - Design Pair

### Tasks for Release:
 - [ ] Finalize research proposal for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [ ] Complete a round of validaiton on [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506) with the devivery team.
 - [ ] Do one final round of reviews on [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [ ] Take a look at [Allow users to promote a deployment to it's own environment on the Environment Index Page
](https://gitlab.com/gitlab-org/gitlab/-/issues/378552).
 - [ ] Continue supporting Jihu on [Add approval settings "Prevent approval by MR merge triggerer" for Protect Environment](https://gitlab.com/gitlab-org/gitlab/-/issues/381418).
 - [ ] Move [New environments search UI (Simplified version for better performance)](https://gitlab.com/gitlab-org/gitlab/-/issues/375244) and [Allow JSON evidence collection files to open up within a separate tab of a users’ web browser
](https://gitlab.com/gitlab-org/gitlab/-/issues/378930) into Workflow::PlanningBreakdown. 

### Other Tasks:
 - [x] Follow up with Gina and Veethika on Coverage Tasks.
 - [ ] Prep for hosting Release Office Hours. 
 - [ ] Finish first draft of Document for Rayana. 
 - [ ] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [ ] Add insights to Dovetail for internail interviews. 

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
