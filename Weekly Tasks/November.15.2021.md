## Week of November 15th, 2021 to November 19th, 2021

I am out of office on vacation in Dubai, Egypt and Jordan. 🌵

- Please contact my manager, [Jacki](https://gitlab.com/jackib) for anything urgent.
- I will be back on **November 30th**.
