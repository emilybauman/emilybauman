## Week of October 18th to October 22nd, 2021

- Taking PTO Tuesday October 19th

## 14.5 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/447)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/437)

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Donut Chat with Tim
 - [x] Watch Recording of Expansion Milestone Kickoff
 - [ ] Watch Recording of Jacki's Team Meeting
 - [x] Watch Recording of Growth Weekly
 - [x] 1:1 with Matej
 - [x] Dev/UX/PM Sync for Expansion
 - [x] Acquisition Team Sync
 - [x] Reforge: Aquisition
 - [x] Emily and Andy Design Pair

### Tasks for Activation:
 - [x] Finish up Illustrations for [Update Illustrations for Survey Response Page](https://gitlab.com/gitlab-org/gitlab-svgs/-/merge_requests/732#note_699084186)
 - [x] Finalize Updates to the Continious Onboarding Flow: [Update continuous onboarding to use invite for help flow
](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/445) and [Update continuous onboarding to use CI/CD walkthrough](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/446)
- [x] Finish gathering feedback on [Make user invitation (with invite for help) it's own dedicated step in first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/441)
- [x] Start work on header updates for logged out users: [Create a marketing header when user is on GitLab and not logged in](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/448) and [Add trial signup to GitLab marketing header
](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/449)


### Tasks for Expansion:
 - [ ] Start working on the screener for [Problem Validation: How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755)
 - [x] Finish gathering feedback and updating [Improve invite modal error alerts when member was already invited](https://gitlab.com/gitlab-org/gitlab/-/issues/334280)
 - [x] Do a quick UX review of [Invite a group to project modal should exclude ancestor groups from Select a group dropdown](https://gitlab.com/gitlab-org/gitlab/-/issues/329835)
 - [ ] Finish up the template issue for [UX: MVC - Non-admins can request to invite users via issue creation and assignment to namespace owner](https://gitlab.com/gitlab-org/gitlab/-/issues/340762)

### Other Tasks:
 - [ ] Continue work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [ ] Finish up Reforge lessons for the week - Activation - Growth Series

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] Urdu Lessons
