## Week of May 30th, 2022 to June 3rd, 2022

## 15.1 Priorities
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/637)

### Key Meetings:
 - [ ] R&D Realignment AMA
 - [ ] UX Weekly Call
 - [ ] 1:1 Emily B. – Matej
 - [ ] Growth Conversion Team Meeting
 - [ ] Emily / Kevin Async
 - [ ] [REC] Growth Design sync
 - [ ] Expansion optional mid milestone catchup
 - [ ] 1:1 Emily B and Jacki
 - [ ] Designer pair: Becka/ Emily

### Tasks for Activation:
 - [ ] Take a look at [Determine barriers to entry at signup (from Arkose risk score)](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/9).
 - [ ] Answer questions for [An admin has an allowlist for the set git_upload_pack settings](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/17).

### Tasks for Expansion:
 - [ ] Finish Up [UX: Trial wind down: in-product messaging and email ahead of trial end for namespaces over 5 user limit
](https://gitlab.com/gitlab-org/gitlab/-/issues/347441).

### Other Tasks:
 - [ ] Keep adding ideas to [Beautifying Our UI 15.2](https://gitlab.com/gitlab-org/gitlab/-/issues/362122)
 
### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
