## Week of February 7, 2022 to February 11th, 2022

## 14.8 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/553)
- [Conversion/Expansion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/547)

### Key Meetings:
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] Jacki's Team Meeting
 - [x] Sync with Matej
 - [x] Aquisitions Team Sync
 - [x] Growth Conversion Team Meeting
 - [ ] Async with Kevin
 - [ ] 1:1 with Jacki

### Tasks for Activation:
 - [ ] Figure out next steps for [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465).
 - [ ] Look at what's next in [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).

### Tasks for Conversion/Expansion:
 - [x] Finish next iteration of design for [Update /billing page to mirror /pricing page design for Free users](https://gitlab.com/gitlab-org/gitlab/-/issues/346272).
 - [x] Restart work on [Indicate how many seats are remaining when namespace approaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/348694).
 - [ ] Get started on [Conversion UX: Create end of trial splash page](https://gitlab.com/gitlab-org/gitlab/-/issues/350167).
 - [ ] Get started on [Wireframes for UXR - Ability for admin to view and control who can access their namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/347438).

### Other Tasks:
 - [ ] Finish Figma File for [Add New Popover with Close Button Variant to Pajamas](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1233).
 - [x] Start conversation with Foundations about Confetti in [Display Invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/351020).
 - [x] Start conversation with Foundations about [Update Country field on Free Trial to Country/Region](https://gitlab.com/gitlab-org/gitlab/-/issues/349333).
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
