## Week of January 17th, 2022 to January 21st, 2022

## 14.8 Priorities
- [Activation Planning Issue TBD]()
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/547) this milestone.

### Key Meetings:
 - [x] Free User Strategy Product/UX Sync
 - [x] UX Weekly
 - [x] Growth Weekly
 - [x] Async with Matej
 - [x] UX Showcase
 - [x] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [x] Growth UX Sync
 - [x] 1:1 with Jacki
 - [x] Design Pair with Andy

### Tasks for Activation:
 - [ ] Finish work on [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465).
 - [ ] TBD based on Milestone Planning

### Tasks for Expansion:
 - [ ] Start on [Update /billing page to mirror /pricing page design for Free users](https://gitlab.com/gitlab-org/gitlab/-/issues/346272)
 - [ ] If time permits, also take a look at [Add Upgrade item to top-right (user profile) drop down for Trial users](https://gitlab.com/gitlab-org/gitlab/-/issues/350168)

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [ ] [Heuristic Buddy Retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/45)
 - [ ] Team Retros 

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
