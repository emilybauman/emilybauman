## Week of November 8th, 2021 to November 12th, 2021

Working remotely in Dubai, may be more async than usual. 

## 14.5 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/447)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/455)

### Key Meetings:
 - [x] Skip Level with Christie (Async)
 - [x] UX Group Conversations 
 - [x] Coffee Chat with Charlie
 - [x] UX Weekly (Async)
 - [x] Growth Weekly (Async)
 - [x] Coffee Chat with Eugie
 - [x] Async with Matej
 - [x] Activation Team Sync
 - [x] UX Showcase
 - [x] 1:1 with Jacki
 - [x] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] Emily and Andy Design Pair 
 - [ ] 1:1 with Kevin
 - [ ] Reforge: User Psychology

### Tasks for Activation:
 - [ ] Finalize issues and ready these issues for development [When pipeline fails due to cc verification failure, Create a persistent header in the UI for that user](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/464) and [Add "verify your identity" flow to first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/452).
 - [ ] Facilitate feedback and finalize work on [Create a marketing header when user is on GitLab and not logged in](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/448).
 - [ ] Finish first design iteration for [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465).

### Tasks for Expansion:
- [ ] Review conversation and help move forward [Conversion Experiment ENG: Move Billing to a top-level navigation item
](https://gitlab.com/gitlab-org/gitlab/-/issues/338540)
 - [ ] Work with Gayle and Nick for next steps on [How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755).
 - [ ] Continue work on [UX Scorecard - Growth:Expansion FY22-Q4: The Invited User Experience](https://gitlab.com/gitlab-org/growth/product/-/issues/1702)
 - [ ] Framiliarize myself with Continuous Onboarding

### Other Tasks:
 - [ ] Continue Work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [x] Finish up Reforge lessons for the week - User Psychology - Growth Series
 - [ ] Start Reforge lessons for next week - Experiments - Growth Series

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] Urdu Lessons
