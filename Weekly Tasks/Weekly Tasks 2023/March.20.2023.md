## Week of March 20th, 2023 to March 24th, 2023

🌴 Slightly short week, OOO for Family & Friends Day on March 24th.

## 15.10 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Environments 15.11 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/1)

### Key Meetings
 - [x] Rayana & Emily 1:1
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] PM Meeting (every 2 weeks) [REC]
 - [x] Emily / Ali
 - [x] UX Showcase
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily / Nico Coffee Chat
 - [x] Emily / Vladimir - coffee chat

### Tasks for Environments:
 - [x] Continue work on [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [ ] Take a look at the filtering aspect of [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901).
 - [ ] Take a look at [Show who is approved to deploy on the environment page and the approval rules](https://gitlab.com/gitlab-org/gitlab/-/issues/383411).
 - [x] Organise design sprint activities.

### Other Tasks:
 - [x] Continue with Onboarding.
 - [x] Prep Design Pair Session.
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
