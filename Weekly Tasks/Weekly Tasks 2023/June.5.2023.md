## Week of June 3rd, 2023 to June 7th, 2023

## 16.1 Priorities
- [Environments 16.1 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/23)

### Key Meetings
 - [x] Customer Call
 - [x] LIVE | GitLab Q1 - Fiscal 2024 Earnings Call
 - [x] Design Sprint Day 1 Sync Session
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA) 
 - [x] ☕ Hunter & Emily
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Emily / Ali
 - [x] CI/CD UX Meeting
 - [x] Design Sprint Day 2 Sync Session
 - [x] Emily & Gina sync
 - [x] FY24-Q1 post-Earnings Internal Recap & AMA

### Tasks for Environments:
 - [x] Continue recruiting participants for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326) - if any are available.
 - [x] Host and participate in Day 1-2 of the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [ ] Finish [Add better feedback when starting a manual deployment action](https://gitlab.com/gitlab-org/gitlab/-/issues/388950).
 - [x] Start recruiting users for Day 3 and the User testing portion of [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).

### Other Tasks:
 - [x] Finish security training.
 - [x] Plan for 16.2
 
### Personal Goals:
 - [x] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
