## Week of July 17th, 2023 to July 21st, 2023

## 16.3 Priorities
- [Environments 16.3 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/32)

### Key Meetings
 - [x] Customer Call
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Emily/Veethika Sync
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1 - Mid Year Review
 - [x] Product Analytics and Feature Flags better together
 - [x] Will/Emily Research Sync
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily / Vladimir - coffee chat
 - [x] Emily & Gina sync

### Tasks for Environments:
 - [x] Start [Design the external job visuals](https://gitlab.com/gitlab-org/gitlab/-/issues/417141).
 - [x] Look into interview guide for [Internal research on populating a service list](https://gitlab.com/gitlab-org/gitlab/-/issues/418208).
 - [x] If time permits, look into [Improve help text around historical releases](https://gitlab.com/gitlab-org/gitlab/-/issues/368309).
 - [ ] Template out Design Sprint Artifacts. 

### Other Tasks:
 - [x] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Continue writing the blog post. 
 - [x] Sync with Veethika around Conference Presentations. 
 - [ ] Regroup with someone from UX around being a Design Sprint POC. 
