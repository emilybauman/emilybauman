## Week of October 30th, 2023 to November 3rd, 2023

## 16.6 Priorities
- [Environments 16.6 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/39)

### Key Meetings
 - [x] Emily / Vladimir - coffee chat
 - [x] Emily / Viktor weekly
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] coffee chat Emily / Olivier
 - [x] ☕ Hunter & Emily
 - [x] UX Showcase
 - [x] Emily / Pedro: Pair design
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily & Gina sync
 - [x] [REC] CI/CD UX Review
 
### Tasks for Environments:
 - [ ] Finish iteration of [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [x] Finish a Scenario for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40) and set up competitor accounts.
 - [ ] Finish off [Update deploy token UX when specifying expired token](https://gitlab.com/gitlab-org/gitlab/-/issues/379050).
 - [x] Finish off [Add context that Release titles take the tag name when there is no title](https://gitlab.com/gitlab-org/gitlab/-/issues/386044).
 - [x] Plan for 16.7.

### Other Tasks:
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
