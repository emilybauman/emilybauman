## Week of February 13th, 2023 to February 17th, 2023

I am out of office on vacation in Guatemala, Honduras, El Salvador and Nicaragua. 🌴

Please contact my manager, [Rayana](https://gitlab.com/rayana) for anything urgent.
I will be back on Tuesday, February 21st, 2023.

