## Week of April 17th, 2023 to April 21st, 2023

🌞 Slightly Short Week - Family & Friends Day on Monday, April 17th, 2023.

## 16.0 Priorities
- [Configure 16.0 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/15)

### Key Meetings
 - [x] Ops AI Initiative Standup
 - [x] State of AI in Development AMA (EMEA/AMER)
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Emily / Ali
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session

### Tasks for Environments:
 - [ ] Finish Prototype and start looking into participants for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] Collect feedback and merge [Add Artifact](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/122868).
 - [x] Start planning for the [Environments Design Sprint](https://gitlab.com/groups/gitlab-org/ci-cd/deploy-stage/environments-group/-/epics/1).
 - [x] Finish [Improve error messages of Protected Environments Setting UI/UX (both Project-level and Group-level)](https://gitlab.com/gitlab-org/gitlab/-/issues/368353).

### Other Tasks:
 - [x] Prep Design Pair Session.
 - [x] Tend to all SUS Severity Label Issues. 
 - [x] Expense Internet Bill.
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
