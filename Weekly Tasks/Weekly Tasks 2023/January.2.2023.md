## Week of January 2nd, 2023 to January 6th, 2023

🌲 Slightly Short Week: Stat Holiday on January 2nd

## 15.8 Priorities
- [15.8 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/202)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] Rayana & Emily 1:1
 - [x] Release UX/FE sync

### Tasks for Release:
 - [x] Finish design for [Provide a way to easily navigate to the Protected Environments setting through the Environments page
](https://gitlab.com/gitlab-org/gitlab/-/issues/378926).
 - [x] Continue working on proposal for [Make navigating to the individual environment page more obvious](https://gitlab.com/gitlab-org/gitlab/-/issues/375610).
 - [x] If time permits, start on [Add Search, Filter, Sort capabilities on the Environment history UI](https://gitlab.com/gitlab-org/gitlab/-/issues/372385).
 - [ ] Finish sourcing participants for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2121).
 - [ ] Launch first version of study for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).

### Other Tasks:
 - [ ] [Notify team of PTO in February](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2186).
 - [x] [Start planning for 15.9](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/203).
 - [x] [Connect Linkedin with Linkedin Recruiter Access](https://gitlab.com/gl-talent-acquisition/operations/-/issues/1433).
 - [ ] Close off [PTO Document for December 23rd to January 2nd](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2175).

### Personal Goals:
 - [ ] [Start looking into FY24 Goals](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
