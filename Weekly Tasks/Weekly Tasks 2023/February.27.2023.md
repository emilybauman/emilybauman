## Week of February 27th, 2023 to March 3rd, 2023

🌴 Slightly Short Week: Family & Friends day on Monday, February 27th.

## 15.10 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Configure 15.10 planning issue](https://gitlab.com/gitlab-org/configure/general/-/issues/331)
- [UX Transition for Configure](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2207)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] [REC] Refigure (Temporary Name) Team Meeting (AMER/EMEA)
 - [x] Toronto get together
 - [x] Emily / Sean ☕ 💬
 - [x] ☕ Mikhail & Emily
 - [x] Emily / Ali
 - [x] ☕ Anna & Emily
 - [x] Rayana & Emily 1:1
 - [x] CI/CD UX Meeting
 - [x] Refigure [Temp Name] - Design Pair Session
 - [x] Customer Call 1
 - [x] ☕ Hunter & Emily
 - [x] Will/Emily Research Sync
 - [x] Andy/Emily Coffee Chat ☕

### Tasks for Refigure (temp name):
 - [ ] Finish reading [Terraform: Up & Running](https://www.terraformupandrunning.com/). 
 - [ ] Finalize research plan for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] Start on [Kubernetes first steps course](https://learnk8s.io/first-steps).
 - [ ] If time permits, take a look at [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901).
 - [x] Coffee chats with new team members. 

### Other Tasks:
 - [x] Prep [Emily Bauman - Other Growth & Development Application](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/1121). 

### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
