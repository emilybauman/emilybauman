## Week of April 24th, 2023 to April 28th, 2023

## 16.0 Priorities
- [Configure 16.0 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/15)

### Key Meetings
 - [x] Customer Call 
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Customer Call
 - [x] Rayana & Emily 1:1 
 - [x] Emily / Ali
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 - [x] Will/Emily Research Sync

### Tasks for Environments:
 - [x] Contact participants for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326) - if any are available.
 - [x] Collect feedback and merge [Add Release](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/122870).
 - [x] Make progress planning for the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [ ] Work on the details view page for [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901/).

### Other Tasks:
 - [x] Prep Design Pair Session.
 - [x] Plan for PTO. 
 - [x] Ask about the state of AI Features.
 - [x] Put time aside to help with Hackathon Reviews.  
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
