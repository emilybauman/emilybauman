## Week of January 30th, 2023 to February 3rd, 2023

## 15.9 Priorities
- [15.9 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/203)

### Key Meetings
 - [x] Release Group Weekly [REC]
 - [x] Release Stage Community Office Hours [REC]
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting
 - [x] Release UX/PM sync
 - [x] Emily / Kevin - Sync
 - [x] Record Kickoff Video

### Tasks for Release:
 - [x] Share out results of [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [x] Finalize flow for [Prevent environment approvals rules from being removed after deployer/approver group is been deleted
](https://gitlab.com/gitlab-org/gitlab/-/issues/381877).
 - [ ] Prep research brief for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] Finalize design for [Make navigating to the individual environment page more obvious](https://gitlab.com/gitlab-org/gitlab/-/issues/375610) and open followup issue.

### Other Tasks:
 - [x] [Finalize 15.10 Planning](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/204).
 - [x] Update computer. 
 - [x] Host Office Hours.
 - [x] Record and share 15.10 UX Kickoff with Chris.

### Personal Goals:
 - [x] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
