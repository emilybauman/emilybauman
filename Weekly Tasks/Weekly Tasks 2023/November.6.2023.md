## Week of October 9th, 2023 to October 13th, 2023

🌴 Short week, OOO on Thursday November 9th and Friday November 10th. 

## 16.6 Priorities
- [Environments 16.6 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/39)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Emily / Sunjung ☕️
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] GitLab Environment Discovery Call
 - [x] CI/CD UX Meeting
 - [x] Will/Emily Research Sync
 
### Tasks for Environments:
 - [ ] Finish off iteration one of [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [x] Record Octopus Walkthrough for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Continue work on Harness or ArgoCD runthroughs for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Record 16.7 Kickoff Video. 

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
 - [x] Expense Internet for September and October. 
