## Week of August 28th, 2023 to August 1st, 2023

## 16.4 Priorities
- [Environments 16.4 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/34)

### Key Meetings
 - [x] Customer Call 1
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Customer Call 2
 - [x] UX Weekly [ASYNC]
 - [x] Customer Call 3
 - [x] Interview
 - [x] Customer Call 4
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily & Gina sync
 - [x] Will/Emily Research Sync

### Tasks for Environments:
 - [x] Recruit users for [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577).
 - [x] Look into [Navigation to Kubernetes Dashboard with No Agent Selected](https://gitlab.com/gitlab-org/gitlab/-/issues/422441) and [Navigation to Kubernetes Dashboard with Agent Connection Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/422443).
 - [ ] Review feedback on [Update New Release Email](https://gitlab.com/gitlab-org/gitlab/-/issues/386074). 
 - [ ] Review feedback on [Design the external job visuals](https://gitlab.com/gitlab-org/gitlab/-/issues/417141).

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [x] Finish 360 Feedback.
 - [x] Do Self Review.
 - [x] Book flight to Denver.
