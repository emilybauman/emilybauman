## Week of May 22nd, 2023 to May 26th, 2023

I am out of office on vacation in the UK, Belgium, Andorra and Spain. 🌴

Please contact my manager, [Rayana](https://gitlab.com/rayana) for anything urgent.
I will be back on Monday, May 28th, 2023.

