## Week of September 25th, 2023 to September 29th, 2023

## 16.5 Priorities
- [Environments 16.5 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/37)

### Key Meetings
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Emily / Nico Coffee Chat
 - [x] Emily / Pedro: Pair design
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Emily / Viktor weekly
 - [x] CI/CD UX Meeting
 - [x] Will/Emily Research Sync
 - [x] Customer Call
 - [x] [REC] Environments - Design Pair Session
 
### Tasks for Environments:
 - [x] Get first iteration done for [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [x] Start looking into [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Make progress on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
 - [x] Review recording and provide Pedro [recomendations](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2359) by Thursday.
