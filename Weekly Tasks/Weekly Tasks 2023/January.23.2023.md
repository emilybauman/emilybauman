## Week of January 23rd, 2023 to January 27th, 2023

## 15.9 Priorities
- [15.9 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/203)

### Key Meetings:
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Release Group Weekly [REC]
 - [x] Release group holiday get together
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] PM Meeting (every 2 weeks) [REC]
 - [x] Rayana & Emily 1:1
 - [x] Release UX/FE sync
 - [x] PM Meeting (every 2 weeks) [REC]
 - [x] Release Stage Brainstorm session [REC]
 - [x] Emily / Vladimir - coffee chat
 - [x] Michael / Emily - Design Pair
 - [x] Will/Emily Research Sync

### Tasks for Release:
 - [x] Finish proposal for [Add Search, Filter, Sort capabilities on the Environment history UI](https://gitlab.com/gitlab-org/gitlab/-/issues/372385).
 - [x] Synthesize data for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [x] Create a flow for [Prevent environment approvals rules from being removed after deployer/approver group is been deleted
](https://gitlab.com/gitlab-org/gitlab/-/issues/381877).
 - [x] Finalize [Create Release Icon](https://gitlab.com/gitlab-org/gitlab-svgs/-/issues/342).
 - [ ] Create first draft of [Link to Environments from Project Home Empty State](https://gitlab.com/gitlab-org/gitlab/-/issues/383898).

### Other Tasks:
 - [x] [Start looking at 15.10 Planning](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/204).
 - [x] [Participate in Team Retro](https://gitlab.com/gl-retrospectives/release/-/issues/47).
 - [x] [Participate in UX Retro](https://gitlab.com/gl-retrospectives/ux-retrospectives/-/issues/64).

### Personal Goals:
 - [ ] [Prepare some high level foals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
