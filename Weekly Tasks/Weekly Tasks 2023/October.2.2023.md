## Week of October 2nd, 2023 to October 6th, 2023

🌴 Slightly short week with the GitLab Product Get Together on October 3rd and 4th & Family and Friends Day on October 6th. 

## 16.5 Priorities
- [Environments 16.5 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/37)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Customer Call
 - [x] [REC] CI/CD UX Review
 - [x] Option 2: FY24-Q3 GitLab Assembly all-company meeting
 
### Tasks for Environments:
 - [x] Get Draft Details View done for [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [ ] Start looking into [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Make progress on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).
 - [x] Get started on [Provide a help text when setting up the cluster UI](https://gitlab.com/gitlab-org/gitlab/-/issues/424493).

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
