## Week of January 16th, 2023 to January 20th, 2023

## 15.8 Priorities
- [15.8 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/202)

## 15.9 Priorities
- [15.9 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/203)

### Key Meetings:
 - [x] Steve - CMS Environment Management
 - [x] Release Group Weekly [REC]
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting
 - [x] Release UX/PM sync
 - [x] Emily / Marcel ☕
 - [ ] Monthly Release Kickoff (Public Livestream)

### Tasks for Release:
 - [ ] Finish proposal for [Add Search, Filter, Sort capabilities on the Environment history UI](https://gitlab.com/gitlab-org/gitlab/-/issues/372385).
 - [x] Finish interviews and synthesis for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2121).
 - [x] Record video and share out results of [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2121).
 - [ ] Synthesize data for [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [ ] Start on 15.9 work such as [Prevent environment approvals rules from being removed after deployer/approver group is been deleted
](https://gitlab.com/gitlab-org/gitlab/-/issues/381877).
 - [x] Create research issue for solution validation of [Add dynamically populated organization-level environments page
](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).

### Other Tasks:
 - [ ] [Start looking at 15.10 Planning](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/204).
 - [x] Data Privacy Training
 - [x] Insider Trader Training
 - [x] Expense Internet for December

### Personal Goals:
 - [ ] [Start looking into FY24 Goals](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
