## Week of December 11th, 2023 to December 15th, 2023

## 16.7 Priorities
- [Environments 16.7 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/45)

### Key Meetings
 - [x] Bonnie & Emily - Coffee Chat
 - [x] Emily / Viktor weekly
 - [x] Emily / Pedro: Pair design
 - [x] UX Weekly Call
 - [x] Emily / Sunjung
 - [x] UX Showcase
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] Emily & Gina sync
 - [x] ☕ Hunter & Emily
 - [x] Rayana & Emily 1:1

 
### Tasks for Environments:
 - [x] Record Kickoff for [16.8](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/47).
 - [ ] Work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).
 - [x] Move [Inconsistent behaviour with environment widgets on MR](https://gitlab.com/gitlab-org/gitlab/-/issues/346666) into refinement.
 - [x] Review [Draft: kas->agentk routing design](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/1198).

### Other Tasks:
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review meetup topic with Veethika. 
