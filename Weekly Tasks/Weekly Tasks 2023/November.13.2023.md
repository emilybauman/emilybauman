## Week of November 13th, 2023 to November 17th, 2023. 

🌴 Short week, OOO on Monday November 13th. 

## 16.6 Priorities
- [Environments 16.6 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/39)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] Emily / Pedro: Pair design
 - [x] UX Weekly Call
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] Emily / Marcel skip level

### Tasks for Environments:
 - [ ] Finish off iteration one of [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [ ] Finish Harness or ArgoCD runthroughs for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Finish off design for  [Add refresh icon for refetching the Kuberentes resources on the Environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/429531).
 
### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
