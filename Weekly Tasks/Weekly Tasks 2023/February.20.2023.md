## Week of February 20th, 2023 to February 24th, 2023

🌴 Slightly Short Week: Stat Holiday on Monday February 20th. 

## 15.10 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Configure 15.10 planning issue](https://gitlab.com/gitlab-org/configure/general/-/issues/331)
- [UX Transition for Configure](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2207)

### Key Meetings
 - [x] Emily / Viktor
 - [x] [REC] Refigure (Temporary Name) Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Emily / Ali
 - [x] Emily & Gina sync
 - [x] Release UX/FE sync
 - [x] Release UX/PM sync
 - [x] [REC] Refigure (Temporary Name) Team Meeting (AMER/APAC)
 - [x] Emily / Nico Coffee Chat
 - [x] Emily / Vladimir - coffee chat
 - [x] DevOps Discussion with Igor

### Tasks for Refigure (temp name):
 - [x] Focus on onboarding tasks for new team. 
 - [x] Set up coffee chats with new team members.
 - [x] Add new Calendar to PTO Deel. 
 - [x] Set up Design Pair meeting.
 - [x] Start on Terraform up and Running. 

### Other Tasks:
 - [ ] Prep [Emily Bauman - Other Growth & Development Application](https://gitlab.com/gitlab-com/people-group/total-rewards/-/issues/1121). 
 - [ ] Close off any existing Release Tasks. 

### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
