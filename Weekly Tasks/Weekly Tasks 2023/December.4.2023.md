## Week of December 4th, 2023 to December 8th, 2023

## 16.7 Priorities
- [Environments 16.7 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/45)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] 📷 Andrei/Emily Coffee Chat
 - [x] Rayana & Emily 1:1
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 - [x] HOLD: Product All-Team Meeting [AMER] [REC] (Quarterly)
 - [x] FY24-Q3 post-Earnings Internal Recap & AMA
 
### Tasks for Environments:
 - [x] Finalize plan for [16.8](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/47).
 - [ ] Work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).
 - [x] Finish [Inconsistent behaviour with environment widgets on MR](https://gitlab.com/gitlab-org/gitlab/-/issues/346666).
 - [ ] Get Slideshow done for UX Showcase.


### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
