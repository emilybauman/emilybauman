## Week of October 9th, 2023 to October 13th, 2023

🌴 Slightly short week, OOO for Canadian Thanksgiving on October 9th.

## 16.5 Priorities
- [Environments 16.5 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/37)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] Emily / Pedro: Pair design
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Emily / Sunjung ☕️
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Customer Call
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily & Gina sync
 
### Tasks for Environments:
 - [x] Continue work on [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [x] Make progress on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).
 - [x] Plan for [16.6](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/39#note_1582467524).
 - [x] Record 16.6 Kickoff Video. 

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
 - [x] Finish up [Coverage Issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2343). 
 - [x] Finish up [FY24 Talent Assessment](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2385) 
