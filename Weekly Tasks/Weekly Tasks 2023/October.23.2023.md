## Week of October 23rd, 2023 to October 27th, 2023

🌴 Slightly short week, OOO for PTO on October 23rd and 24th

## 16.6 Priorities
- [Environments 16.6 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/39)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] Emily / Pedro: Pair design
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] Product Direction Showcase [EMEA/AMER] (Private Livestream)
 - [x] [REC] CI/CD UX Review
 - [x] Will/Emily Research Sync
 
### Tasks for Environments:
 - [x] Continue work on [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [ ] Start on [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [ ] If time permits, look at [Update deploy token UX when specifying expired token](https://gitlab.com/gitlab-org/gitlab/-/issues/379050).

### Other Tasks:
 - [x] Finish off Self Assessment. 
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
 - [ ] Catch up on todos from PTO. 
