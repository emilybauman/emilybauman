## Week of November 27th, 2023 to December 1st, 2023

🌞 Slightly Short Week - Family & Friends Day on Monday, November 27th, 2023.

## 16.7 Priorities
- [Environments 16.7 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/45)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Emily / Pedro: Pair design
 - [x] UX Showcase
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 
### Tasks for Environments:
 - [x] Close off any last tasks for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Get [Add context that Release titles take the tag name when there is no title](https://gitlab.com/gitlab-org/gitlab/-/issues/386044) ready for a first round of reviews.
 - [x] Plan for [16.8](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/47).
 - [x] Work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).


### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
