## Week of March 27th, 2023 to March 31th, 2023

## 15.11 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Configure 15.11 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/1)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Customer Call
 - [x] Rayana & Emily 1:1
 - [x] Emily / Ali
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 - [x] #random-coffees-ux-dept Donut
 - [x] Will/Emily Research Sync

### Tasks for Environments:
 - [x] Continue work on [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [ ] Take a look at the filtering aspect of [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901).
 - [ ] Take a look at [Show who is approved to deploy on the environment page and the approval rules](https://gitlab.com/gitlab-org/gitlab/-/issues/383411).

### Other Tasks:
 - [x] Continue with Onboarding.
 - [x] Prep Design Pair Session.
 - [x] Prep a small presenation for CI/CD UX Meeting.
 - [x] Prep for Customer Call. 
 - [x] Organize Calendar for Week in May.
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
