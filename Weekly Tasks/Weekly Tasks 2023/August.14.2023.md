## Week of August 14th, 2023 to August 18th, 2023

## 16.3 Priorities
- [Environments 16.3 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/32)

### Key Meetings
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly [ASYNC]
 - [x] Rayana & Emily 1:1 
 - [x] Emily / Pedro: Pair design
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session

### Tasks for Environments:
 - [ ] Recruit users for [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577).
 - [x] Record 16.4 Kickoff and share in [Kickoff Issue](https://gitlab.com/gitlab-com/Product/-/issues/12417).
 - [x] Review a few UX Debt Environments Issues. 

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Continue reviews for [Blog Post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34383). 
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [x] Expense Internet.
