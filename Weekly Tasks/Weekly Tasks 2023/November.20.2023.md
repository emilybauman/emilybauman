## Week of November 20th, 2023 to November 24th, 2023

## 16.7 Priorities
- [Environments 16.7 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/45)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Jess:Emily ☕
 - [x] Customer Call
 - [x] Rayana & Emily 1:1
 - [x] Customer Call
 - [x] [REC] Environments - Design Pair Session
 
### Tasks for Environments:
 - [x] Record Harness and Octopus Walkthroughs for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Finish ArgoCD initial walkthrough for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [x] Record ArgoCD initial walkthrough for [Competitor Evaluation - Environments](https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/40).
 - [ ] Finish a proposal for [Strange error message when group milestones are associated to a release](https://gitlab.com/gitlab-org/gitlab/-/issues/277397).
 - [ ] If time permits, work on [Add context that Release titles take the tag name when there is no title](https://gitlab.com/gitlab-org/gitlab/-/issues/386044).


### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
