## Week of June 19th, 2023 to June 23rd, 2023

🌴 Short week with Family and Friends day on June 23rd. 

## 16.2 Priorities
- [Environments 16.1 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/31)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1 & IGP Checkin
 - [x] CI/CD UX Meeting
 - [x] Emily / Ali
 - [x] [REC] Environments - Design Pair Session
 - [x] Engineering All Hands (EMEA/AMER) [REC]
 - [x] Option 2: FY24-Q2 GitLab Assembly all-company meeting
 - [x] GitLab Live Event

### Tasks for Environments:
 - [x] Start putting together a testing plan for Day 4 of [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Review Glossary for [Service](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/126005) term. 
 - [x] Look into [Paginate the section for deploy keys in project settings](https://gitlab.com/gitlab-org/gitlab/-/issues/407604).

### Other Tasks:
 - [x] Finish next section of [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Go through new interview training.  
 - [x] Start prepping UX Showcase Presenation for the Design Sprint. 
 - [x] Expense May Internet.
