## Week of July 31st, 2023 to August 4th, 2023

## 16.3 Priorities
- [Environments 16.3 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/32)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1 
 - [x] CI/CD UX Meeting
 - [x] Interview
 - [x] Emily / Marcel skip level
 - [x] [REC] Environments - Design Pair Session
 - [x] ☕ Hunter & Emily

### Tasks for Environments:
 - [x] Finish designs for [Design the external job visuals](https://gitlab.com/gitlab-org/gitlab/-/issues/417141).
 - [x] Continue working on [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577).
 - [x] Plan for 16.4

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Finish first draft of [Blog Post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34383). 
 - [x] Template out Design Sprint Artifacts. 
 - [x] Complete neurodiversity training. 
