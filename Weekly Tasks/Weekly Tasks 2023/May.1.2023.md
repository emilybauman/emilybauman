## Week of May 1st, 2023 to May 5th, 2023

## 16.0 Priorities
- [Configure 16.0 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/15)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1 
 - [x] Emily / Ali
 - [x] Emily / Nico Coffee Chat
 - [x] Emily / Marcel skip level
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] ☕ Hunter & Emily

### Tasks for Environments:
 - [x] Continue recruiting participants for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326) - if any are available.
 - [x] Continue planning for the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Continue work on [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901/).

### Other Tasks:
 - [x] Prep Design Pair Session.
 - [x] Plan for PTO. 
 - [x] Prep a screener for Viktor.
 - [x] Try to recruit design sprint participants. 
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
