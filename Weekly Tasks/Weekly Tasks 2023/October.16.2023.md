## Week of October 16th, 2023 to October 20th, 2023

I am out of office on vacation in Turkey. 🌴

Please contact my manager, [Rayana](https://gitlab.com/rayana) for anything urgent.
I will be back on Wednesday, October 25th, 2023.

