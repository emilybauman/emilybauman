## Week of August 21st, 2023 to August 25th, 2023

🌴 Short week with a PTO Day on Wednesday, August 23rd and Family & Friends Day on Friday, August 25th.

## 16.4 Priorities
- [Environments 16.4 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/34)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Vladimir - coffee chat
 - [x] Emily / Viktor weekly
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly [ASYNC]
 - [x] Rayana & Emily 1:1 
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review

### Tasks for Environments:
 - [x] Recruit users for [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577).
 - [ ] Finish proposal for [UX feedback when limit reached alert interaction on button click](https://gitlab.com/gitlab-org/gitlab/-/issues/268053).
 - [x] Look into [Navigation to Kubernetes Dashboard with No Agent Selected](https://gitlab.com/gitlab-org/gitlab/-/issues/422441) and [Navigation to Kubernetes Dashboard with Agent Connection Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/422443).
 - [ ] Review feedback on [Update New Release Email](https://gitlab.com/gitlab-org/gitlab/-/issues/386074). 
 - [ ] Review feedback on [https://gitlab.com/gitlab-org/gitlab/-/issues/417141](https://gitlab.com/gitlab-org/gitlab/-/issues/417141).

### Other Tasks:
 - [x] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Continue reviews for [Blog Post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34383). 
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
