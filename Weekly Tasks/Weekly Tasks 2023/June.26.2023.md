## Week of June 26th, 2023 to June 30th, 2023

## 16.2 Priorities
- [Environments 16.1 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/31)

### Key Meetings
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Emily / Viktor weekly
 - [x] UX Weekly Call
 - [x] Emily / Ali
 - [x] UX Showcase
 - [x] Rayana & Emily 1:1 & Growth Plan Convo
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily & Gina sync
 - [x] Will/Emily Research Sync

### Tasks for Environments:
 - [ ] Finish first iteration of testing plan for review and start on prototype for [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [x] Finish design for [Paginate the section for deploy keys in project settings](https://gitlab.com/gitlab-org/gitlab/-/issues/407604).
 - [x] Start looking into the design for [Add better feedback when starting a manual deployment action](https://gitlab.com/gitlab-org/gitlab/-/issues/388950).

### Other Tasks:
 - [x] Finish next section of [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Finish interview training.
 - [x] Finish UX Showcase Presentation.
 - [x] Start on Mid-Year Review Document. 
 - [ ] Adjust Goals based on Feedback. 
