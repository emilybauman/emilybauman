## Week of April 10th, 2023 to April 14th, 2023

## 15.11 Priorities
- [Emily Bauman: Learn the Delivery domain tech](https://gitlab.com/gitlab-org/configure/general/-/issues/334)
- [Emily Bauman’s UX transition and onboarding to the Delivery stage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2210)
- [Configure 15.11 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/1)

### Key Meetings
 - [x] ☕ Timo & Emily
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] CD workflow with GitLab Release product team
 - [x] Emily / Ali
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 - [x] Rayana & Emily 1:1

### Tasks for Environments:
 - [ ] Finish Prototype for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] Finish Filter Concept for [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901).
 - [x] Collect feedback and merge [Adds Application, Configuration, Secrets and Deployment](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/122652).
 - [x] Finish 16.0 Planning. 
 - [x] Record 16.0 Kickoff Video. 
 - [x] Make adjustments to MVC of [Deployment Detail Page MVC - Deploy Approval](https://gitlab.com/gitlab-org/gitlab/-/issues/374538) for Andrew.
 - [x] Create [some mockups](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/issues/326#note_1347930947) for Anna. 

### Other Tasks:
 - [x] Continue with Onboarding.
 - [x] Prep Design Pair Session.
 - [x] [Coverage tasks](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2227) for Veethika. 
 - [x] 15.11 Environments Retro. 
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
