## Week of September 11th, 2023 to September 15th, 2023

## 16.4 Priorities
- [Environments 16.4 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/34)

### Key Meetings
 - [x] Rayana & Emily 1:1
 - [x] GitLab | Docebo - Multi-project Pipelines Discussion
 - [x] CI/CD UX Meeting
 - [x] Emily / Pedro: Pair design
 - [x] [REC] Environments - Design Pair Session
 - [ ] Elaine // Emily
 
### Tasks for Environments:
 - [x] Close off research for [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577) and conduct some interviews.
 - [x] Start working on MVC Designs for [Service oriented operations: Design and IA](https://gitlab.com/gitlab-org/gitlab/-/issues/417184).
 - [x] Finish concept for [Navigation to Kubernetes Dashboard with Agent Connection Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/422443).

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [x] Review list of [conferences to speak at](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85#note_1525577858) and choose one.
 - [x] Plan for 16.5.
