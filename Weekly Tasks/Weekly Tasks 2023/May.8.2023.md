## Week of May 8th, 2023 to May 12th, 2023

🌴 Slightly short week, OOO on May 12th for some PTO.

## 16.0 Priorities
- [Environments 16.0 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/15)

### Key Meetings
 - [x] Skip Level: UX CI/CD
 - [x] User Interview
 - [x] UX Weekly Call
 - [x] Emily and Viktor Meeting Hold
 - [x] Rayana & Emily 1:1
 - [x] Emily / Ali
 - [x] [REC] Environments - Design Pair Session

### Tasks for Environments:
 - [x] Continue recruiting participants for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326) - if any are available.
 - [x] Finish recruitment survey for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] Finalize prototype for [Solution Validation: Group-Level Production Environments](https://gitlab.com/gitlab-org/ux-research/-/issues/2326).
 - [x] Continue planning for the [Environments Design Sprint](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/11).
 - [ ] Film a few Design Sprint activity videos. 
 - [ ] Continue work on [GitLab built-in Kubernetes dashboard Design](https://gitlab.com/gitlab-org/gitlab/-/issues/365901/).

### Other Tasks:
 - [x] Prep Design Pair Session.
 - [x] Plan for PTO. 
 - [x] Finish planning 16.1 and film kickoff.
 
### Personal Goals:
 - [ ] [Prepare some high level goals for FY24](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2176). 
