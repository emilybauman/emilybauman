## Week of August 7th, 2023 to August 11th, 2023

🌴 Short week with a Public Holiday on Monday, August 7th.

## 16.3 Priorities
- [Environments 16.3 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/32)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] All Product Meeting: Engagement Survey Results
 - [x] UX Weekly [ASYNC]
 - [x] Rayana & Emily 1:1 
 - [x] UX Showcase
 - [x] Emily & Gina sync

### Tasks for Environments:
 - [x] Finish designs for [Design the external job visuals](https://gitlab.com/gitlab-org/gitlab/-/issues/417141).
 - [ ] Recruit users for [Solution Validation: External Research on Populating a Service List](https://gitlab.com/gitlab-org/ux-research/-/issues/2577).
 - [ ] Plan for 16.4

### Other Tasks:
 - [ ] Refine goals and finalize off the [IGP](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2306).
 - [x] Get first round of reviews out for [Blog Post](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/34383). 
 - [x] Template out Design Sprint Artifacts. 
 - [ ] Review [Kickoff Tasks](https://gitlab.com/gitlab-com/Product/-/issues/12417). 
