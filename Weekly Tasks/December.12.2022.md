## Week of December 12th, 2022 to December 16th, 2022

## 15.7 Priorities
- [15.7 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/174)

### Key Meetings:
 - [x] GitLab Delivery Team Continuous Interviews
 - [x] Release Group Weekly [REC]
 - [x] PM Meeting (every 2 weeks) [REC]
 - [x] AMA with Sid Sijbrandij (CEO) (Public Live Stream)
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] Rayana & Emily 1:1
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Record Kickoff Video
 - [x] Halil | Emily ☕
 - [x] 1:1 Emily B and Jacki
 - [x] Emily / Vladimir - coffee chat
 - [ ] Michael / Emily - Design Pair

### Tasks for Release:
 - [ ] Finish and launch first step of [Solution Validation: Deployment Details Page](https://gitlab.com/gitlab-org/ux-research/-/issues/2205).
 - [x] Continue to gather feedback on [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [x] Help with UX Sandbox setup of [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [ ] Get first iteration done for [Allow users to promote a deployment to it's own environment on the Environment Index Page
](https://gitlab.com/gitlab-org/gitlab/-/issues/378552).
 - [x] Record 15.8 Kickoff. 

### Other Tasks:
 - [ ] Tend to feedback on the document for Rayana. 
 - [x] Create issues out of [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425) and pass off to Daniel.
 - [x] Finish 15.8 Plan. 
 - [x] Open coverage issue for winter holdays. 

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
