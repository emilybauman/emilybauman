## Week of November 22nd, 2021 to November 26th, 2021

I am out of office on vacation in Dubai, Egypt and Jordan. 🌵

- Please contact my manager, [Jacki](https://gitlab.com/jackib) for anything urgent.
- I will be back on **November 30th**.
