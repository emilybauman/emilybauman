## Week of August 23rd to August 27th, 2021

### Key Meetings:
 - [x] Donut Chat with Monika
 - [x] 1:1 with Jacki
 - [x] Jacki's Team Meeting
 - [ ] Growth Weekly
 - [ ] UX Weekly

### Tasks for Activation:
 - [ ] Complete a draft screener survey and problem validation issue for [Project Orchestration idea](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/418).
 - [ ] Continue to clean up the [Experience Recommendations](https://gitlab.com/gitlab-org/growth/product/-/issues/1691) for the Verify Onboarding Scorecard.


### Tasks for Expansion:
 - [ ] Start looking at sythesis of [Allow non-admins to request access to a feature/tier or recommend an invite](https://gitlab.com/gitlab-org/ux-research/-/issues/1297).
 - [ ] Write up a first iteration of the survey for [Problem validation: Invited user onboarding](https://gitlab.com/gitlab-org/ux-research/-/issues/1009).

### Other Tasks:
 - [x] Ongoing work around [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [ ] Make [Growth L&D Recommendations](https://gitlab.com/gitlab-org/growth/ui-ux/-/issues/102) reccomendations when I find things. 

### Personal Goals:
 - [x] See what actions I can take on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [x] Read up on new things from my Reforge Cohort.
 
### Issue Backlog:

**To Be Done**
- [Experiment - Add expiration copy to invited user email](https://gitlab.com/gitlab-org/gitlab/-/issues/337860)
- [Experiment - Notify non-admin users to contact admin to invite new users to namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/337555)
- [Pitch a paid plan when users click to purchase CI minutes](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/420)

**Designs complete, waiting on implementation**
- [Provide admins with a sharable invite signup URL issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336240/)
- [Add "welcome" questions to namespace creation UX](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/392).
- [Switch Easy-Button Icons with Radio Buttons](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/400).
- [Link CI Onboarding MR Widget to the Pipeline Editor](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/399).
- [Ask admin (inviter) what areas they'd like the invitee to focus on](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Notify inviter if invite email hard bounces](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Allow users to invite others onto GitLab to help complete a task](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/393).
- [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/336248)
- [Net Improvement - Fix Invite modal copy to accurately communicate "access expires" impact](https://gitlab.com/gitlab-org/gitlab/-/issues/337865). 
- [Add guided walkthrough to pipeline editor](https://gitlab.com/gitlab-org/gitlab/-/issues/334659)
