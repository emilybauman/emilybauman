## Week of April 4th, 2022 to April 8th, 2022

## 14.10 Priorities
- [Activation/Adoption Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/619)
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/618)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/621)

### Key Meetings:
 - [x] Coffee Chat with Kamil
 - [x] Growth Weekly
 - [x] UX Weekly Call
 - [x] Coffee Chat with Doug
 - [x] Async with Kevin
 - [x] Coffee Chat with Alper
 - [x] Growth Design: UX Sync
 - [x] Conversion #gametime
 - [x] Reforge | Solution Ideation
 - [x] 1:1 Emily B and Jacki
 - [x] Design Pair with Becka

### Tasks for Activation:
 - [x] Finish off [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).
 - [ ] Finish off [Add walkthrough/wizard for the standard CI template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/572).

### Tasks for Expansion:
 - [x] Finish off [UX: Trial start: Inform owner they can add as many users as they want in the trial period; note return to X users and free plan at end](https://gitlab.com/gitlab-org/gitlab/-/issues/347440).
 - [ ] If time permits get started on [UX: Trial wind down: in-product messaging and email ahead of trial end for namespaces over 5 user limit
](https://gitlab.com/gitlab-org/gitlab/-/issues/347441).

### Other Tasks:
 - [x] Finish Week 2 of Reforge Content.

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
