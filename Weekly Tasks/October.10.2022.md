## Week of October 10th, 2022 to October 14th, 2022

🌴 Short week - Canadian Thankgiving on Monday, October 10th. 

## 15.5 Priorities
- [15.5 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/158)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] UX Weekly Call
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] UX Showcase
 - [x] Release Stage Brainstorm session
 - [x] Rayana & Emily 1:1
 - [x] Record Kickoff Video
 
### Tasks for Release:
 - [x] Launch [Solution Validation: Validate users are okay being notified of Pending Deployment Approvals with To-Dos](https://gitlab.com/gitlab-org/ux-research/-/issues/2070).
 - [ ] Validate JTBD for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [ ] Finish off design for [Provide a mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/-/issues/19724).
 - [x] Finish off UX plan for 15.6.
 - [x] Clean up [Creating a new tag is not clear on new release page](https://gitlab.com/gitlab-org/gitlab/-/issues/367893).

### Other Tasks:
 - [x] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [ ] If time permits, help Eugie with the [Arkose Proposal](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/96). 

### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
