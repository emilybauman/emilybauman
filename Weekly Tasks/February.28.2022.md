## Week of February 28th, 2022 to March 4th, 2022

## 14.9 Priorities
- [Activation/Adoption Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/569)
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/568)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/570)

### Key Meetings:
 - [x] UX Weekly
 - [x] Growth Weekly
 - [x] Async with Matej
 - [x] Activation Team Meeting
 - [x] UX Showcase
 - [x] 1:1 with Gayle
 - [x] 1:1 with Kevin
 - [x] Growth Design UX Sync
 - [x] 1:1 with Jacki
 - [x] 1:1 with Jensen

### Tasks for Activation:
 - [ ] Finish up [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).
 - [x] Close off [Contact Sales Email 1](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/573) and [Sales Email 2](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/571).
 - [ ] If time allows, get started on [Add walkthrough/wizard for the standard CI template](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/572).

### Tasks for Expansion:
 - [x] Finish up work on [UX: lock invite options when namespace reaches user limit](https://gitlab.com/gitlab-org/gitlab/-/issues/348697/).
 - [x] Get started on [For existing free namespaces over 5 users, notify Owner which users will stay and which will be removed](https://gitlab.com/gitlab-org/gitlab/-/issues/349943).
 - [x] Continue work on [Wireframes for UXR - Ability for admin to view and control who can access their namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/347438) if it becomes unblocked this week.

### Other Tasks:
 - [x] Close off [Add New Popover with Close Button Variant to Pajamas](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1233).
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [x] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
