## Week of February 21st, 2022 to February 25th, 2022

Short Week - Monday and Friday are off.

## 14.9 Priorities
- [Activation/Adoption Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/569)
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/568)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/570)

### Key Meetings:
 - [x] UX Weekly
 - [ ] 1:1 with Matej
 - [x] Activation Team Meeting
 - [x] Growth Conversion Team Meeting
 - [ ] Async with Kevin
 - [ ] 1:1 with Jacki
 - [ ] 1:1 with Nick
 - [ ] Design Pair with Becka

### Tasks for Activation:
 - [ ] Finish up [Add is_creating_ios_app to project profile](https://gitlab.com/groups/gitlab-org/growth/-/epics/93).
 - [ ] Get started on [Contact Sales Email 1](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/573) and [Sales Email 2](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/571).
 - [ ] If time allows, get started on [Add integrate GitLab with Test Flight to continuous onboarding (when is_creating_ios_app is true)](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/513).

### Tasks for Expansion:
 - [ ] Continue work on [Wireframes for UXR - Ability for admin to view and control who can access their namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/347438).
 - [ ] If time allows, get started on [For existing free namespaces over 5 users, notify Owner which users will stay and which will be removed](https://gitlab.com/gitlab-org/gitlab/-/issues/349943).

### Other Tasks:
 - [ ] Close off [Add New Popover with Close Button Variant to Pajamas](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1233).
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
