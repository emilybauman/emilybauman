## Week of December 6th, 2021 to December 10th, 2021

## 14.6 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/504)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/505) this milestone.

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] UX Weekly Call
 - [x] Growth Weekly
 - [x] Async with Matej
 - [x] Design Pair with Andy
 - [x] Acquisitions Team Sync
 - [x] UX Showcase
 - [x] 1:1 with Gayle
 - [x] 1:1 with Jensen
 - [x] 1:1 with Kevin
 - [x] Growth Social

### Tasks for Activation:
 - [x] Start work on [Add confirm identity via phone number to validation in first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/501)
 - [ ] Take a look at [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465)

### Tasks for Expansion:
 - [x] Start work on [Add high conversion trial actions to Continuous onboarding](https://gitlab.com/gitlab-org/gitlab/-/issues/345174)
 - [x] Take a look at [Experiment UX: during cart abandonment offer trial and talk to sales option
](https://gitlab.com/gitlab-org/gitlab/-/issues/343179)
- [ ] Review discussion guide for [Billing/Purchase Page Discoverability Unmoderated Test](https://docs.google.com/document/d/1EBfO84Jdta6KGdwx_SQ-rQLlevNcWmvbuSkg4tIN4XI/edit?usp=sharing)

### Other Tasks:
 - [x] Finish work on Free User Strategy
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [x] Start work on heuristic buddy tasks on [Manage:Optimize FY22-Q4 - Evaluating VSA](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12593)

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
