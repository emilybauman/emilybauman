## Week of September 12th, 2022 to September 16th, 2022

I am out of office on vacation in Finland, Sweden and Norway. 🌴

Please contact my manager, [Rayana](https://gitlab.com/rayana) for anything urgent.
I will be back on September 21st.

