## Week of June 6th, 2022 to June 10th, 2022

## 15.1 and 15.2 Priorities
- Focus this Milestone on Transitioning into Release
- [Career Mobility Emily Bauman, per 2022-06-13 as Product Designer](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4157)
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Emily / Rayana
 - [x] Release Group Weekly [REC]
 - [x] UX Weekly Call
 - [x] #random-coffees-ux-dept Donut
 - [x] 1:1 Emily B and Jacki
 - [x] Coffee Chat ☕ Chris/Emily
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Coffee Chat ☕ Ahmed/Emily
 - [x] UX buddy 1:1 check-in
 - [x] Coffee Chat ☕ Nicole/Emily
 - [x] Coffee Chat ☕ Allen/Emily
 - [x] Coffee Chat ☕ Katie/Emily
 
### Tasks for Release:
 - [ ] Finish up first week tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Close off any tasks in [Career Mobility Emily Bauman, per 2022-06-13 as Product Designer](https://gitlab.com/gitlab-com/team-member-epics/employment/-/issues/4157).

### Tasks for Growth:
 - [x] Close off any outstanding Conversion/Expansion Tasks.
 - [x] Finish Up [Determine barriers to entry at signup (from Arkose risk score)](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/9).

### Other Tasks:
 - [x] Continue work on [Beautifying Our UI 15.2](https://gitlab.com/gitlab-org/gitlab/-/issues/362122).
 - [x] Get started on my [Mid-Year Check-In](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2002). 
 
### Personal Goals:
 - [ ] Fix up [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
