## Week of September 19th, 2022 to September 23rd, 2022

🌴 Short week - PTO on Monday the 19th and Tueday the 20th.

## 15.5 Priorities
- [15.5 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/158)

### Key Meetings:
 - [x] Emily / Viktor: say hi
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Rayana & Emily 1:1
 - [x] Release Stage Community Office Hours [HOLD]
 - [x] Chris / Will / Rayana / Emily - Monthly Check-in
 
### Tasks for Release:
 - [ ] Start on [Improve Empty State Pages for Deployments](https://gitlab.com/gitlab-org/gitlab/-/issues/371392).
 - [ ] Continue work on [Solution Validation: Validate users are okay being notified of Pending Deployment Approvals with To-Dos](https://gitlab.com/gitlab-org/ux-research/-/issues/2070).
 - [ ] Finish agenda for [Release Stage Community Office Hours - 2022-09-22](https://gitlab.com/gitlab-org/gitlab/-/issues/371708).
 - [ ] If time permits, take a look at [Don't automatically run a deployment job post approval when using Unified Approval Setting](https://gitlab.com/gitlab-org/gitlab/-/issues/367943).

### Other Tasks:
 - [ ] Catch up from PTO. 
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
