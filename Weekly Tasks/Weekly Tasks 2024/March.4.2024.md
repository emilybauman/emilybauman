## Week of March 4th, 2024 to March 8th, 2024

🌴 - PTO on Friday, March 8th.

## 16.10 Priorities
- [Environments 16.10 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/66)

### Key Meetings
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] Emily / Austin <> Design Pair
 - [x] Will/Emily Research Sync
 - [x] Q4 Results
 - [x] Emily / Viktor weekly
 - [x] Emily & Gina sync
 - [x] [REC] UX Call
 - [x] Rayana & Emily 1:1
 - [ ] [REC] Environments - Design Pair Session
 
### Tasks for Environments:
 - [ ] Continue work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839) again. 
 - [ ] Finish design for [Add a sticky head to Environment Details Page](https://gitlab.com/gitlab-org/gitlab/-/issues/439766).
 - [x] Record [16.11 Kickoff](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/67).

### Other Tasks:
 - [x] UX Buddy Tasks. 
 - [x] Finish tasks for [Actionable Insights Workshop: Conversational pairworking with AI](https://gitlab.com/gitlab-org/ux-research/-/issues/2901).
 - [x] Prep for Summit.
 - [x] Expense January & February Internet Bills. 
