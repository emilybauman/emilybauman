## Week of June 3rd, 2024 to June 7th, 2024

## 17.1 Priorities
- [Import and Integrate 17.1 Plan](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17673)
- [Environments 17.1 Plan](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/75)

### Key Meetings
 - [x] LIVE | GitLab Q1 - FY2025 Earnings Call
 - [x] Emily / Viktor weekly
 - [x] Emily / Austin <> Design Pair
 - [x] Emily | Taylor Coffee Chat ☕
 - [x] Rayana & Emily 1:1
 - [x] Emily / Will - Sync Chat
 - [x] Katie / Emily - AI Settings Sync
 - [x] [Americas] CI/CD UX Meeting
 - [x] Pam & Emily coffee chat
 - [x] Emily / Pedro: Pair design
 - [x] Anna & Emily Coffee Chat
 - [x] ☕ Hunter & Emily
 - [x] Emily | Jacki ☕

### Tasks for Import and Integrate:
 - [x] Provide engineering support when needed. 
 - [x] Tend to the following issues: 
      - [x] Close off [Show in the project on source instance that the project was migrated](https://gitlab.com/gitlab-org/gitlab/-/issues/439714).
 - [x] Close off any tasks before the borrow to AI Settings.

### Tasks for Environments:
 - [x] Close off any tasks before the borrow to AI Settings. 

### Other Tasks:
 - [ ] UX Buddy Tasks. 
 - [ ] AI Settings Onboarding. 