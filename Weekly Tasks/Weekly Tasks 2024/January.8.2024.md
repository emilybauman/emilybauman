## Week of January 8th, 2024 to January 12th, 2024

🌴 Slightly short week, F&F Day on Friday, January 12th.

## 16.8 Priorities
- [Environments 16.8 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/47)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Viktor weekly
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] Emily / Vladimir - coffee chat
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [ ] Coffee Chat 
 
### Tasks for Environments:
 - [x] Launch and Synthesize data from [Solution Validation: Kubernetes Dashboard Views](https://gitlab.com/gitlab-org/ux-research/-/issues/2829).
 - [ ] Continue work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).
 - [ ] Get first iteration done for [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671).
 - [x] Finish off [Update environment page to use status icons instead of pipeline status icons](https://gitlab.com/gitlab-org/gitlab/-/issues/432374).
 - [x] Film [16.9 Kickoff](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/53). 

### Other Tasks:
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
