## Week of January 15th, 2024 to January 19th, 2024

## 16.9 Priorities
- [Environments 16.9 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/53)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] UX Weekly Call
 - [x] Rayana & Emily 1:1
 - [x] CI/CD UX Meeting
 - [x] [REC] Environments - Design Pair Session
 
### Tasks for Environments:
 - [ ] Continue work on [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671).
 - [ ] Start up work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839) again. 
 - [x] Plan next steps for [Kubernetes Dashboard Tree View](https://gitlab.com/gitlab-org/gitlab/-/issues/438279).
 - [x] Take a look at [Move the cluster UI component under the environment detail page](https://gitlab.com/gitlab-org/gitlab/-/issues/431746).

### Other Tasks:
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [x] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
