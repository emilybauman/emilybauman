## Week of August 5th, 2024 to August 9th, 2024

🌴 Slightly Short Week - OOO Monday for a Public Holiday. 

## 17.3 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] [REC] AI-Powered Monthly - Option 1
 - [x] [REC] UX Call
 - [x] Early access program discussion Ross & Emily
 - [x] Kevin & Emily Coffee Chat
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Rayana & Emily 1:1
 - [x] AI Framework - UX & FE Sync
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] Emily / Nico Coffee Chat
 - [x] AI UX Weekly Share (EMEA)

### Tasks for AI Settings:
 - [x] Continue work on [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
 - [ ] Work on illustration for [[UX] UX and copy requirements for combined Ultimate + Duo Enterprise trial registration](https://gitlab.com/gitlab-org/gitlab/-/issues/468027/).
 - [ ] Review [UX needs - AI Framework | Duo Workflow (WIP)](https://gitlab.com/gitlab-org/gitlab/-/issues/476516).
 - [x] Fix up content in [UX + Copy Review for /trials/duo_pro/new](https://gitlab.com/gitlab-org/gitlab/-/issues/469420/).
 - [ ] Finish [UX New users: Add Duo Enterprise trial creation to existing Ultimate trial new user registration flow for Gitlab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/473814). 

### Other Tasks:
 - [ ] Review Environments and Import and Integrate design needs if capacity allows. 
    - [ ] [Add a sticky head to Environment Details Page](https://gitlab.com/gitlab-org/gitlab/-/issues/439766).
    - [ ] [Add a description text field to environment details](https://gitlab.com/gitlab-org/gitlab/-/issues/450944).
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).