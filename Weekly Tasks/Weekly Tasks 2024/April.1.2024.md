## Week of April 1st, 2024 to April 5th, 2024

🌴 - Slightly Short Week, Family & Friends day on March 5th. 

## 16.11 Priorities
- [Import and Integrate Borrow](https://gitlab.com/gitlab-com/Product/-/issues/13128)

### Key Meetings
 - [x] Emily & Rayana | Career Check-in
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] [REC] UX Call
 - [x] ✨ Product Design studio - Session 1
 - [x] Magdalena | Emily - Visual Indicator Testing
 - [x] Rayana & Emily 1:1
 - [x] Emily x2 chat
 - [x] UX Showcase
 - [x] Emily / Martin
 - [x] Emily / Viktor
 - [x] Import and Integrate | Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] User mapping sync
 
### Tasks for Import and Integrate:
 - [x] Finalize user flow for [User contribution mapping](https://gitlab.com/groups/gitlab-org/-/epics/12378) and start on UI.
 - [x] Conduct a round of testing for [BE - Add visual indicators for content imported by direct transfer](https://gitlab.com/gitlab-org/gitlab/-/issues/424454).
 - [ ] Finish off design for [BE - Add visual indicators for content imported by direct transfer](https://gitlab.com/gitlab-org/gitlab/-/issues/424454).
 - [ ] If time allows, take a look at the following issues:
      - [x] [Improve discoverability of import results](https://gitlab.com/gitlab-org/gitlab/-/issues/431178).
      - [ ] [Provide clear and actionable steps when showing import failures to users](https://gitlab.com/gitlab-org/gitlab/-/issues/441411).
      - [ ] [Make import results filterable and searchable](https://gitlab.com/gitlab-org/gitlab/-/issues/441405).
      - [ ] [Show in the project on source instance that the project was migrated](https://gitlab.com/gitlab-org/gitlab/-/issues/439714).

### Other Tasks:
 - [x] UX Buddy Tasks. 
 - [x] Review feedback for [JTBD OKR](https://gitlab.com/gitlab-org/gitlab/-/issues/442669). 
 - [x] Have a discussion on [FY24 - Emily Bauman - Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/81) and work on next steps.
 - [x] Prep for PTO.
