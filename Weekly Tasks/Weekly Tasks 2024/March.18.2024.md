## Week of March 18th, 2024 to March 22nd, 2024

🌴 - Slightly Short Week, PTO on Monday March 18th & Tuesday March 19th

## 16.11 Priorities
- [Import and Integrate Borrow](https://gitlab.com/gitlab-com/Product/-/issues/13128)

### Key Meetings
 - [x] Emily / Magdalena
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] Rodrigo | Emily Coffee Chat
 - [x] Emily / Austin <> Design Pair
 - [x] [REC] CI/CD UX Review
 
### Tasks for Import and Integrate:
 - [x] Review [User contribution mapping](https://gitlab.com/groups/gitlab-org/-/epics/12378) and get started on a plan. 
 - [x] Review [BE - Add visual indicators for content imported by direct transfer](https://gitlab.com/gitlab-org/gitlab/-/issues/424454).
 - [ ] If time allows, take a look at the following issues:
      - [ ] [Improve discoverability of import results](https://gitlab.com/gitlab-org/gitlab/-/issues/431178).
      - [ ] [Provide clear and actionable steps when showing import failures to users](https://gitlab.com/gitlab-org/gitlab/-/issues/441411).
      - [ ] [Make import results filterable and searchable](https://gitlab.com/gitlab-org/gitlab/-/issues/441405).
      - [ ] [Show in the project on source instance that the project was migrated](https://gitlab.com/gitlab-org/gitlab/-/issues/439714).

### Other Tasks:
 - [x] UX Buddy Tasks. 
 - [x] Finish tasks for step 2 of [Actionable Insights Workshop: Conversational pairworking with AI](https://gitlab.com/gitlab-org/ux-research/-/issues/2901).
 - [ ] [JTBD OKR](https://gitlab.com/gitlab-org/gitlab/-/issues/442669). 