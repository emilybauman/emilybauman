## Week of February 19th, 2024 to February 23rd, 2024

Slight Short Week - OOO for Family Day (Ontario) on February 19th. 

## 16.10 Priorities
- [Environments 16.10 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/66)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Sunjung - Design Sprint ☕️
 - [x] [REC] UX Call
 - [x] Rayana & Emily 1:1
 - [x] UX Showcase
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] Emily/Veethika Bi-weekly Sync
 
### Tasks for Environments:
 - [x] Get started on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839) again. 
 - [x] Finish [Environment logs and events streaming visualisation - Design](https://gitlab.com/gitlab-org/gitlab/-/issues/391514).
 - [ ] Work on [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671) if time permits. 

### Other Tasks:
 - [ ] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
 - [x] Finish UX Showcase Presentation.
 - [x] Finish SAFE Training.
 - [x] Onboarding Buddy Tasks.
