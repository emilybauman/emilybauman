## Week of December 2nd, 2024 to December 6th, 2024

## 17.7 Priorities
- AI Settings 
- Q
- Beautifying our UI

### Key Meetings
 - [x] Emily / Viktor - Coffee Chat
 - [x] Anna & Emily Coffee Chat
 - [x] Data Science section UX meetup
 - [x] Emily / Jacki
 - [x] LIVE | GitLab Q3 - FY2025 Earnings Call 
 
### Tasks for AI Settings:
 - [x] Review MRs and support engineering with Settings Related Work. 
 - [x] Discuss with design and product the roadmap for next year with AI Framework. 

### Tasks for Q: 
 - [x] Review outstanding designs. 
      - [x] [Support "Disconnect" for Q integration](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/232).
      - [x] [Add Health Check to GitLab Duo with Q Settings](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/278)
 
### Other Tasks:
 - [ ] Complete more Beautifying our UI issues:
     - [ ] [Re-evaluate Kubernetes agent registration dialog](https://gitlab.com/gitlab-org/gitlab/-/issues/506444).
     - [x] [Surface deployment information to the environment list view without using collabsibles](https://gitlab.com/gitlab-org/gitlab/-/issues/505770).
     - [ ] [Improve UX of registering a GitLab Agent](https://gitlab.com/gitlab-org/gitlab/-/issues/349809).
     - [ ] [Show the previous (successful) deployment job on the deployment details page](https://gitlab.com/gitlab-org/gitlab/-/issues/498077).
 - [ ] Shift Nudge Tasks
      - [x] Finish watching videos. 
      - [ ] Finish Week 4 & 5 Homework. 