## Week of January 1st, 2024 to January 5th, 2024

🌴 Slightly short week, OOO for New Years Day on January 1st. 

## 16.8 Priorities
- [Environments 16.8 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/47)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Customer Call 1
 - [x] Customer Call 2
 
### Tasks for Environments:
 - [x] Gather feedback on [Solution Validation: Kubernetes Dashboard Views](https://gitlab.com/gitlab-org/ux-research/-/issues/2829).
 - [ ] Continue work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839).
 - [x] Move [Show a visual for stopping environments](https://gitlab.com/gitlab-org/gitlab/-/issues/431913) into refinement. 
 - [x] Get first iteration done for [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671).
 - [x] If time permits, finish [Update environment page to use status icons instead of pipeline status icons](https://gitlab.com/gitlab-org/gitlab/-/issues/432374).

### Other Tasks:
 - [ ] Finish up the remaining issue templates for [Template Design Sprint Artifacts](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2326).
 - [ ] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
