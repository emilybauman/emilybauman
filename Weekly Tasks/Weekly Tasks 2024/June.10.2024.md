## Week of June 10th, 2024 to June 14th, 2024

Slightly Short Week - OOO for Friends and Family Day on June 14th.

## 17.2 Priorities
- AI Settings.

### Key Meetings
 - [x] Pini | Emily ☕
 - [x] Emily / Austin <> Design Pair
 - [x] ☕ Timo & Emily
 - [x] Torsten | Emily ☕
 - [x] Deploy:Environments get together EMEA / AMER
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] [REC] UX Call
 - [x] Gayle | Emily ☕
 - [x] Emily & Katie - AI Onboarding Sync
 - [x] Rayana & Emily 1:1
 - [x] Timothy | Emily ☕
 - [x] David | Emily ☕
 - [x] Emily / Paige
 - [x] [REC] CI/CD UX Review

### Tasks for AI Settings:
 - [ ] Get Duo working locally following [our docs](https://docs.gitlab.com/ee/development/ai_features/#get-started).
     - [x] Open MRs to fix docs if needed. 
 - [x] Create a 17.2 Milestone Plan. 
    - Will work in weekly plans, not monthly. 
 - [x] Review work in flight. 

### Other Tasks:
 - [x] UX Buddy Tasks. 
 - [x] Start on Mid Year Review.