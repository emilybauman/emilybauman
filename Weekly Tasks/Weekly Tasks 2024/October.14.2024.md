## Week of October 14th, 2024 to October 18th, 2024

🌴 Slightly Short Week - OOO Monday for a Public Holiday.

## 17.5 Priorities
- AI Settings 
- Duo Trials
- Q

### Key Meetings
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Emily / Viktor - Coffee Chat
 - [x] Jackie / Emily Sync
 - [x] Fast Boot wrap up and next steps
 - [x] [REC] UX Call
 - [x] Emily / Jacki
 - [x] UX Social Runthrough
 - [x] AI Framework - UX & FE Sync
 - [x] UX Forum
 - [x] UX team social AMER
 - [x] Patrick / Emily Design Sync

### Tasks for AI Settings:
 - [ ] Support engineering with [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
      - [ ] Banner Design for [Remove Duo Settings from Admin Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493456). 
      - [ ] Banner Design for [Remove Duo Settings from Group Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493457).

### Tasks for Q:
- [x] Organize what work is needed after FastBoot. 
      - [ ] Continue work on [[UX] Follow Up for Admin Set Up Flow](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/105).
      - [ ] Support Patrick with UX Reviews. 
      - [ ] Start on future state of Amazon Q.

### Other Tasks:
 - [ ] Start on Growth Plan for next year. 
 - [ ] Shift Nudge Tasks
      - [ ] Finish Week 4 & 5 Homework. 
 - [x] Prepare UX Forum Presentation.
 - [x] Host UX Socials. 