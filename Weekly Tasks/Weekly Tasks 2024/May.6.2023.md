## Week of May 6th, 2024 to May 10th, 2024

## 17.0 Priorities
- [Import and Integrate 17.0 Plan](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17672)
- [Environments 17.0 Plan](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/73)

### Key Meetings
 - [x] Emily / Pedro: Pair design
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Mikhail | Emily Coffee Chat
 - [x] Emily | Marcel - JTBD Catchup
 - [x] Import and Integrate | Design Pair Session
 - [x] Pitchbook - GitLab on Flux and app delivery
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] Emily & Gina sync
 
### Tasks for Import and Integrate:
 - [x] Finish CSV Proposal for [User contribution mapping](https://gitlab.com/groups/gitlab-org/-/epics/12378) and finalize any existing flows.
 - [x] Finish off synthesis and close off [Solution Validation: User Mapping](https://gitlab.com/gitlab-org/ux-research/-/issues/2953).
 - [x] If time allows, take a look at the following issues:
      - [x] [Improve discoverability of import results](https://gitlab.com/gitlab-org/gitlab/-/issues/431178).

### Tasks for Environments:
 - [ ] Work on [Think through the Flux bootstrap command UX and related arguments](https://gitlab.com/gitlab-org/gitlab/-/issues/454546).
 - [x] Start looking into [Create new jobs for Environments](https://gitlab.com/gitlab-org/gitlab/-/issues/458797).
 - [ ] Provide OKR feedback. 
 - [ ] Install and explore Flux if time permits. 
 - [x] Record Kickoff.

### Other Tasks:
 - [x] UX Buddy Tasks. 
 - [x] Coverage for Rayana. 
 - [x] Code of Conduct Course. 