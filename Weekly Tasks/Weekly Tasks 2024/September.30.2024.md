## Week of September 30th, 2024 to October 4th, 2024

## 17.5 Priorities
- AI Settings 
- Duo Trials
- Q

### Key Meetings
 - [x] AI Framework - Weekly Meeting [REC]
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] [REC] AI-Powered Monthly - Option 1
 - [x] Patrick / Emily Design Sync
 - [x] Jackie / Emily Sync
 - [x] Emily / Gayle / Paige - UX / PM Sync
 - [x] Emily / Libor (design pair)
 - [x] Emily & Rayana ☕
 - [x] Review UX of Assign Q items
 - [x] AI Framework - UX & FE Sync
 - [x] Anna & Emily Coffee Chat
 - [x] Emily / Nico Coffee Chat
 - [x] AWS Q Product Deep Dive Prep for Fast Boot
 - [x] Option 1: FY25-Q3 GitLab Assembly all-company meeting
 - [x] Will/Emily ☕

### Tasks for AI Settings:
 - [ ] Support engineering with [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
      - [ ] Banner Design for [Remove Duo Settings from Admin Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493456). 
      - [ ] Banner Design for [Remove Duo Settings from Group Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493457).

### Tasks for Growth:
 - [ ] Continue work on [Add 'toggle' to Step 6 of Trial Registration flow to allow for invited users to be assigned a Duo Seat](https://gitlab.com/gitlab-org/gitlab/-/issues/480524).

### Tasks for Q:
- [x] Organize Fast Boot Plan. 
- [x] Finish [UX: Assign issue to Q to create a task and open an MR](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/17).
- [x] Take a look at [UX: Upgrade a Java project](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/18).
- [x] Finish design for [UX GitLab Duo Settings for AWS/Amazon Q Bundle](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/32).

### Other Tasks:
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [ ] Shift Nudge Tasks
      - [x] Finish watching Week 5 Material. 
      - [ ] Finish Week 5 Homework. 
 - [ ] Summzarize Learnings from Open Source Summit. 