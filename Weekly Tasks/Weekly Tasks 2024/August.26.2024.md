## Week of August 26th, 2024 to August 30th, 2024

🌴 Slightly Short Week - OOO Monday.

## 17.4 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] Emily / Viktor - Coffee Chat
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] AI Framework - UX & FE Sync
 - [x] Emily / Libor (design pair)
 - [x] Emily | Tiger Coffee Chat
 - [x] Anna & Emily Coffee Chat
 - [x] [Social Time 🥳] CI/CD UX Meeting
 - [x] Emily / Pini catch up
 - [x] Will/Emily ☕
 - [x] Emily / Jacki / Rayana
 - [x] Jessie / Emily ☕
 - [x] Emily / Denys ☕


### Tasks for AI Settings:
 - [ ] Support engineering with [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
 - [x] Finish [UX New users: Add Duo Enterprise trial creation to existing Ultimate trial new user registration flow for Gitlab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/473814) when logo conversation concludes.
 - [x] Organize next steps for [Step 7 Registration Flow updates to support Duo Enterprise Trial](https://gitlab.com/gitlab-org/gitlab/-/issues/479567).
 - [x] Get started on [Active and End Trial Widgets support Duo trials](https://gitlab.com/gitlab-org/gitlab/-/issues/478075).
 - [x] Look at [Trial Discover Page support all Duo Trials](https://gitlab.com/gitlab-org/gitlab/-/issues/478259).

### Other Tasks:
 - [x] Review Environments design needs if capacity allows. 
    - [x] [Add a description text field to environment details](https://gitlab.com/gitlab-org/gitlab/-/issues/450944).
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [ ] Shift Nudge Tasks
      - [x] Finish watching Week 2 Material. 
      - [ ] Finish Week 2 Homework. 
 - [x] [Emily Bauman - Growth & Development Application - Open Source Summit Europe](https://gitlab.com/gitlab-com/people-group/learning-development/growth-and-development-requests/-/issues/249)
      - [x] Buy Flight