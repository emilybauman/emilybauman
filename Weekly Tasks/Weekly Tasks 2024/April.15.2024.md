## Week of April 15th, 2024 to April 19th, 2024

🌴 - Slightly Short Week, OOO on Monday, April 15th.

## 17.0 Priorities
- [Import and Integrate 17.0 Plan](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17672)
- [Environments 17.0 Plan](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/73)

### Key Meetings
 - [x] [REC] UX Call
 - [x] Carla | Emily Coffee Chat
 - [x] Emily / Austin <> Design Pair
 - [x] Import and Integrate | Design Pair Session
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 - [x] Emily & Gina sync
 
### Tasks for Import and Integrate:
 - [x] Continue refining the UI for [User contribution mapping](https://gitlab.com/groups/gitlab-org/-/epics/12378).
 - [x] Start on plan for [Solution Validation: User Mapping](https://gitlab.com/gitlab-org/ux-research/-/issues/2953).
 - [x] Answer final questions related to [BE - Add visual indicators for content imported by direct transfer](https://gitlab.com/gitlab-org/gitlab/-/issues/424454).
 - If time allows, take a look at the following issues:
      - [x] [Improve discoverability of import results](https://gitlab.com/gitlab-org/gitlab/-/issues/431178).

### Tasks for Environments:
 - [ ] Review [Think through the Flux bootstrap command UX and related arguments](https://gitlab.com/gitlab-org/gitlab/-/issues/454546).
 - [x] Review feedback for [JTBD OKR](https://gitlab.com/gitlab-org/gitlab/-/issues/442669) and apply changes. 
 - If time allows, take a look at the following issues:
      - [x] [Add a sticky head to Environment Details Page](https://gitlab.com/gitlab-org/gitlab/-/issues/439766).

### Other Tasks:
 - [x] Insider Trading Training. 
 - [x] UX Buddy Tasks. 
 - [x] Catch up from PTO. 
 - [x] Review [Add a way to delete extra active pages deployments](https://gitlab.com/gitlab-org/gitlab/-/issues/443641).