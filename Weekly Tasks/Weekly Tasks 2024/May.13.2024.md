## Week of May 13th, 2024 to May 17th, 2024

## 17.1 Priorities
- [Import and Integrate 17.1 Plan](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17673)
- [Environments 17.1 Plan](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/75)

### Key Meetings
 - [x] Emily / Austin <> Design Pair
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] [REC] UX Call
 - [x] Import and Integrate | Design Pair Session
 - [x] [REC] Environments - Design Pair Session
 - [x] [REC] CI/CD UX Review
 
### Tasks for Import and Integrate:
 - [x] Move [User contribution mapping](https://gitlab.com/groups/gitlab-org/-/epics/12378) over to engineering.
 - [ ] If time allows, take a look at the following issues:
      - [x] [Improve discoverability of import results](https://gitlab.com/gitlab-org/gitlab/-/issues/431178).
      - [ ] [Make import results filterable and searchable](https://gitlab.com/gitlab-org/gitlab/-/issues/441405).

### Tasks for Environments:
 - [ ] Review feedback and finalize the last two jobs for [Create new jobs for Environments](https://gitlab.com/gitlab-org/gitlab/-/issues/458797).
 - [x] Review design needs for [Make Resource Groups more reliable and flexible](https://gitlab.com/groups/gitlab-org/-/epics/6774).
 - [ ] Get started with [Flux](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/82)
 - [x] Provide OKR feedback. 
  - [x] If time allows, take a look at the following issues:
      - [x] [Deployment Approvals UI does not scale well to small viewports](https://gitlab.com/gitlab-org/gitlab/-/issues/438419).

### Other Tasks:
 - [x] UX Buddy Tasks. 
 - [x] Coverage for Rayana - CI/CD Review Meeting. 