## Week of October 28th, 2024 to November 1st, 2024

I am out of office on vacation in Africa. 🌴

Please contact my manager, [Jacki](https://gitlab.com/jackib) for anything urgent.
I will be back on Tuesday, November 18th, 2024
