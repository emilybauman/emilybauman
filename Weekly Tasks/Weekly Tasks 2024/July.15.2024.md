## Week of July 15th, 2024 to July 19th, 2024

## 17.3 Priorities
- AI Settings

### Key Meetings
 - [x] ☕ Timo & Emily
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Rayana & Emily - Mid Year Checkin
 - [x] Jacki | Emily Chat
 - [x] Emily / Viktor weekly
 - [x] Andrei & Emily Monthly ☕
 - [x] Customer Call
 - [x] AI Framework - UX & FE Sync
 - [x] [Americas] CI/CD UX Meeting
 - [x] Support <> AI Framework Monthly Sync
 - [x] Rayana & Emily 1:1
 - [x] Customer Call

### Tasks for AI Settings:
 - [x] Finish interivews for [Solution Validation: Unified Duo Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/3080).
 - [x] Start synthesis on [Solution Validation: Unified Duo Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/3080).
 - [x] Finalize flow for [[UX] New duo enterprise flows for Gitlab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/468044/). 
 - [x] Get started on flow for [UX + Copy Review for /trials/duo_pro/new](https://gitlab.com/gitlab-org/gitlab/-/issues/469420).

### Other Tasks:
 - [ ] Review Environments and Import and Integrate design needs if capacity allows. 
 - [x] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).