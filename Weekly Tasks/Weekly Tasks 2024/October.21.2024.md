## Week of October 21st, 2024 to October 25th, 2024

## 17.6 Priorities
- AI Settings 
- Q

### Key Meetings
 - [x] Andrew & Emily Monthly ☕
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Q Future State Sync
 - [x] Emily / Libor (design pair)
 - [x] AI Framework - UX & FE Sync
 - [x] Emily & Sunjung ☕
 - [x] Patrick / Emily Design Sync
 - [x] Emily / Pini catch up
 - [x] Support <> AI Framework Sync
 - [x] Anna & Emily Coffee Chat
 - [x] Emily / Jacki
 - [x] UX team social AMER

### Tasks for AI Settings:
 - [x] Support engineering with [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
      - [x] Banner Design for [Remove Duo Settings from Admin Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493456). 
      - [x] Banner Design for [Remove Duo Settings from Group Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493457).

### Tasks for Q: 
 - [x] Finalize flow for [[UX] Follow Up for Admin Set Up Flow](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/105).
 - [x] Support Patrick with UX Reviews. 
 - [x] Review future state ideas with Jacki. 

### Other Tasks:
 - [x] Finalize Growth Plan for FY26.  
 - [ ] Shift Nudge Tasks
      - [ ] Finish Week 4 & 5 Homework. 
 - [x] Host UX Socials. 