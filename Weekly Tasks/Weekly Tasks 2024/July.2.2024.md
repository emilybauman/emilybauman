## Week of July 2nd, 2024 to July 5th, 2024

🌴 Slightly Short Week - OOO Monday for a Public Holiday.

## 17.3 Priorities
- AI Settings

### Key Meetings
 - [x] Emily & Katie - AI Onboarding Sync
 - [x] Emily / Viktor weekly
 - [x] Emily / Gayle Connect - Duo Enterprise trials
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Rayana & Emily - Mid Year Checkin
 - [x] ☕ Hunter & Emily
 - [x] [Americas] CI/CD UX Meeting
 - [x] Anna & Emily Coffee Chat
 - [x] Frédéric & Emily ☕

### Tasks for AI Settings:
 - [x] Finalize prototype and follow up with AI Recruiting Panel for [Solution Validation: Unified Duo Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/3080).
 - [ ] **ON HOLD** Review [Early Access (beta) program setting - ux only](https://gitlab.com/gitlab-org/gitlab/-/issues/460093) if unblocked.
 - [x] Review work needed for [[Growth] Duo Enterprise Trial Registration experience for Gitlab.com](https://gitlab.com/groups/gitlab-org/-/epics/14136). 
      - [x] Get Staging Account.
      - [ ] Review Austin's recording. 

### Other Tasks:
 - [x] Provide reviews. 
 - [x] UX Important Problems Survey. 
 - [x] Review Environments and Import and Integrate design needs if capacity allows. 
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).