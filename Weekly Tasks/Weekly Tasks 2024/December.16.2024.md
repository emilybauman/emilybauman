## Week of December 16th, 2024 to December 20th, 2024

## 17.8 Priorities
- Duo Chat
- Amazon Q
- Beautifying our UI

### Key Meetings
 - [x] Duo Chat Weekly
 - [x] Q Analytics in GitLab Visualization
 - [x] UX Forum
 - [x] Emily / Paul - UX toggle for experiments
 - [x] Patrick / Emily Design Sync
 - [x] Andrei & Emily Monthly ☕
 - [x] Lindsey / Emily Coffee Chat
 - [x] Option 2: FY25-Q4 GitLab Assembly all-company meeting
 - [x] AI UX Share (AMER)
 - [x] Support <> AI Framework Sync
 - [x] Anna & Emily Coffee Chat
 - [x] Data Science section UX meetup
 - [x] Emily / Jacki
 - [x] Jackie / Emily Sync
 
### Tasks for Duo Chat:
 - [ ] Finalize first iteration of design for [DRAFT: Iteration 2: Multi-threaded conversations - List & CRUD (in IDE & GitLab Web Page)](https://gitlab.com/groups/gitlab-org/-/epics/16108).

### Tasks for Q: 
 - [x] Support Patrick with the upcoming Research. 
 - [x] Review Q needs for Q Anayltics for GitLab.
 
### Other Tasks:
 - [ ] Complete more Beautifying our UI issues:
     - [x] [Improve UX of registering a GitLab Agent](https://gitlab.com/gitlab-org/gitlab/-/issues/349809).
     - [ ] [Show the previous (successful) deployment job on the deployment details page](https://gitlab.com/gitlab-org/gitlab/-/issues/498077).
     - [ ] [Add call-to-action on Project settings > CI/CD page for features that are only available with an upgraded license](https://gitlab.com/gitlab-org/gitlab/-/issues/389451).
 - [ ] Shift Nudge Tasks
      - [ ] Finish Week 4 & 5 Homework. 
 - [x] Prepare for UX Showcase.