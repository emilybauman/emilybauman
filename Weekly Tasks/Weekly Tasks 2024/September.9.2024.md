## Week of August 26th, 2024 to August 30th, 2024

🌴 Slightly Short Week - OOO Thursday for PTO and Friday for Family and Friends Day

## 17.4 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] Emily / Viktor - Coffee Chat
 - [x] [Async] Emily / Austin / Tim <> AI Design Pair
 - [x] Emily / Gayle / Paige - UX / PM Sync
 - [x] Emily / Jacki
 - [x] Sync on Assign to Q flow
 - [x] Jackie / Emily ☕
 - [x] Patrick / Emily Design Sync
 - [x] Growth Team Meeting

### Tasks for AI Settings:
 - [x] Support engineering with [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
 - [ ] Finish up [Add 'toggle' to Step 6 of Trial Registration flow to allow for invited users to be assigned a Duo Seat](https://gitlab.com/gitlab-org/gitlab/-/issues/480524).
 - [x] Help with content for [Duo Enterprise + Chat Free-access Cut-off: In-product alert for paid Gitlab.com customers](https://gitlab.com/gitlab-org/gitlab/-/issues/482067). 
 - [x] Assign to Q Flows. 

### Other Tasks:
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [ ] Shift Nudge Tasks
      - [x] Finish watching Week 4 Material. 
      - [ ] Finish Week 4 Homework. 
 - [x] Prep for Open Source Summit. 