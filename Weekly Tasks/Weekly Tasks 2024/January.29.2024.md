## Week of January 29th, 2024 to February 2nd, 2024

## 16.9 Priorities
- [Environments 16.9 planning issue](https://gitlab.com/gitlab-org/ci-cd/deploy-stage/environments-group/general/-/issues/53)

### Key Meetings
 - [x] Emily / Viktor weekly
 - [x] [REC] Environments Team Meeting (AMER/EMEA)
 - [x] Rayana & Emily 1:1
 - [x] CI/CD UX Meeting
 - [x] Pam & Emily coffee chat
 - [x] Emily / Marcel skip level
 - [x] [REC] Environments - Design Pair Session
 - [x] ☕ Hunter & Emily

 
### Tasks for Environments:
 - [ ] Start on [Show the release process related to environments](https://gitlab.com/gitlab-org/gitlab/-/issues/432671).
 - [x] Continue work on [Environment Stage - JTBDs](https://gitlab.com/gitlab-org/gitlab/-/issues/419839) again. 
 - [x] Get an iteration done for [Deployment Approval Rules in Group-level protected environments setting UI](https://gitlab.com/gitlab-org/gitlab/-/issues/362236).
 - [x] Start thinking about next milestone plan. 

### Other Tasks:
 - [ ] Template [Competitor Evaluation FigJam File](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2416). 
