## Week of March 25th, 2024 to March 29th, 2024

🌴 - Slightly Short Week, Good Friday on Friday. 

## 16.11 Priorities
- [Import and Integrate Borrow](https://gitlab.com/gitlab-com/Product/-/issues/13128)

### Key Meetings
 - [x] Emily / Austin <> Design Pair
 - [x] Rayana & Emily 1:1
 - [x] Emily / Sunjung - Design Sprint recap & follow-up
 - [x] CI/CD UX Meeting
 - [x] #random-coffees-ux-dept Donut
 - [x] Import and Integrate | Design Pair Session
 - [x] Emily & Gina sync
 
### Tasks for Import and Integrate:
 - [x] Finalize user flow map for [User contribution mapping](https://gitlab.com/groups/gitlab-org/-/epics/12378) and start on UI.
 - [x] Get first iteration done for [BE - Add visual indicators for content imported by direct transfer](https://gitlab.com/gitlab-org/gitlab/-/issues/424454).
 - [ ] If time allows, take a look at the following issues:
      - [ ] [Improve discoverability of import results](https://gitlab.com/gitlab-org/gitlab/-/issues/431178).
      - [ ] [Provide clear and actionable steps when showing import failures to users](https://gitlab.com/gitlab-org/gitlab/-/issues/441411).
      - [ ] [Make import results filterable and searchable](https://gitlab.com/gitlab-org/gitlab/-/issues/441405).
      - [ ] [Show in the project on source instance that the project was migrated](https://gitlab.com/gitlab-org/gitlab/-/issues/439714).

### Other Tasks:
 - [ ] UX Buddy Tasks. 
 - [x] [JTBD OKR](https://gitlab.com/gitlab-org/gitlab/-/issues/442669). 
 - [x] Work on [FY24 - Emily Bauman - Career Development Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/81).
