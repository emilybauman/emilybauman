## Week of July 8th, 2024 to July 12th, 2024

## 17.3 Priorities
- AI Settings

### Key Meetings
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] Pam & Emily coffee chat
 - [x] Andrew & Emily Monthly ☕
 - [x] AI Framework - Weekly Meeting [REC]
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] David & Emily - ☕️
 - [x] Duo Settings - Solution Validation Session
 - [x] [REC] UX Call
 - [x] Coffee Chat - Buck + Emily
 - [x] Customer Call
 - [x] Emily / Ben - Coffee and Settings
 - [x] Emily / Pedro: Pair design
 - [x] AI Framework - UX & FE Sync
 - [x] AI UX office hours (EMEA)
 - [x] Emily / Nico Coffee Chat
 - [x] [REC] CI/CD UX Review
 - [x] Customer Call
 - [x] Customer Call
 - [x] Customer Call
 - [x] Customer Call

### Tasks for AI Settings:
 - [x] Conduct interviews for [Solution Validation: Unified Duo Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/3080) as they come in.
 - [ ] ~~Review [Early Access (beta) program setting - ux only](https://gitlab.com/gitlab-org/gitlab/-/issues/460093) if unblocked.~~
 - [x] Craft an updated flow for [[UX] New duo enterprise flows for Gitlab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/468044/). 
 - [x] Review what new Growth work should be taken on. 

### Other Tasks:
 - [x] Review Environments and Import and Integrate design needs if capacity allows. 
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).