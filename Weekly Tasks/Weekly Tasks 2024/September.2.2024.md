## Week of August 26th, 2024 to August 30th, 2024

🌴 Slightly Short Week - OOO Monday for Labour Day (Canada).

## 17.4 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] [REC] UX Call
 - [x] LIVE | GitLab Q2 - FY2025 Earnings Call
 - [x] AI UX Weekly Share (EMEA)
 - [x] [REC] AI-Powered Monthly - Option 1
 - [x] ☕ Hunter & Emily
 - [x] Emily / Jacki
 - [x] Andrew & Emily Monthly ☕

### Tasks for AI Settings:
 - [x] Support engineering with [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
 - [x] Finish [Active and End Trial Widgets support Duo trials](https://gitlab.com/gitlab-org/gitlab/-/issues/478075) and move to engineering. 
 - [x] Finish template for [Trial Discover Page support all Duo Trials](https://gitlab.com/gitlab-org/gitlab/-/issues/478259) and move to engineering. 
 - [x] Organize work left for [Update trial 'global alert' to drive group owners/admins to group settings](https://gitlab.com/gitlab-org/gitlab/-/issues/479567).

### Other Tasks:
 - [x] Review Environments design needs if capacity allows. 
    - [x] [Add a description text field to environment details](https://gitlab.com/gitlab-org/gitlab/-/issues/450944).
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [ ] Shift Nudge Tasks
      - [x] Finish watching Week 3 Material. 
      - [ ] Finish Week 3 Homework. 
 - [x] Security Training