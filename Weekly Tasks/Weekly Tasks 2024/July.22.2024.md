## Week of July 22nd, 2024 to July 26th, 2024

## 17.3 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] AI Framework - Weekly Meeting [REC]
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Emily & Gayle - UX / PM Sync
 - [x] Customer Call (Optional)
 - [x] Emily / Austin (Working Session)
 - [x] Pam & Emily coffee chat
 - [x] Customer Call (Optional)
 - [x] Emily/Veethika Bi-weekly Sync
 - [x] [REC] UX Call
 - [x] Rayana & Emily 1:1
 - [x] AI UX office hours (EMEA)
 - [x] Divya | Emily Onboarding Buddy Chat
 - [x] Will/Emily Research Sync


### Tasks for AI Settings:
 - [x] Close off [Solution Validation: Unified Duo Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/3080).
 - [x] Start on [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
 - [x] Finalize location of banners in [UX: Define group /billings page states per tier for Duo trial offerings](https://gitlab.com/gitlab-org/gitlab/-/issues/472087/). 
 - [x] Finalize flow and get started on illustration for [[UX] UX and copy requirements for combined Ultimate + Duo Enterprise trial registration](https://gitlab.com/gitlab-org/gitlab/-/issues/468027/).

### Other Tasks:
 - [x] Review Environments and Import and Integrate design needs if capacity allows. 
    - [x] [List last events at the bottom of Kubernetes resource details](https://gitlab.com/gitlab-org/gitlab/-/issues/470041).
    - [x] [Clean up the UI for the protected environments settings](https://gitlab.com/gitlab-org/gitlab/-/issues/470588).
    - [x] Create Modal for [Discussion: Direct Transfer Member Import and User Mapping](https://gitlab.com/gitlab-org/gitlab/-/issues/466118#note_2010301114).
 - [x] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).