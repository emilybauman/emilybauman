## Week of August 19th, 2024 to August 23rd, 2024

🌴 Slightly Short Week - OOO Friday.

## 17.4 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] ☕ Timo & Emily
 - [x] Emily / Nico Coffee Chat
 - [x] AI Framework - Weekly Meeting [REC]
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Emily & Gayle - UX / PM Sync
 - [x] Andrei & Emily Monthly ☕
 - [x] [REC] UX Call
 - [x] Emily / Libor (design pair)
 - [x] Rayana & Emily 1:1
 - [x] AI Framework - UX & FE Sync
 - [x] AI UX Weekly Share (EMEA)
 - [x] ☕ Coffee Chat: Emily / Alyssa
 - [x] [REC] CI/CD UX Review
 - [x] Emily / Jacki

### Tasks for AI Settings:
 - [x] Clean up issue and move [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637) over to engineering.
 - [ ] Finish [UX New users: Add Duo Enterprise trial creation to existing Ultimate trial new user registration flow for Gitlab.com](https://gitlab.com/gitlab-org/gitlab/-/issues/473814) when logo conversation concludes.
 - [x] Start looking at [Step 7 Registration Flow updates to support Duo Enterprise Trial](https://gitlab.com/gitlab-org/gitlab/-/issues/479567).
 - [x] Get started on [Active and End Trial Widgets support Duo trials](https://gitlab.com/gitlab-org/gitlab/-/issues/478075).

### Other Tasks:
 - [ ] Review Environments design needs if capacity allows. 
    - [ ] [Add a description text field to environment details](https://gitlab.com/gitlab-org/gitlab/-/issues/450944).
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [x] Shift Nudge Tasks
      - [x] Finish watching Week 1 Material. 
      - [x] Finish Week 1 Homework. 
 - [ ] [Emily Bauman - Growth & Development Application - Open Source Summit Europe](https://gitlab.com/gitlab-com/people-group/learning-development/growth-and-development-requests/-/issues/249)
      - [x] Buy Conference Ticket.
      - [ ] Buy Flight
      - [x] Buy Hotel 
 - [x] [✨ Pilot: CI/CD Usability Improvement Week ✨](https://gitlab.com/gitlab-org/gitlab/-/issues/474379)
      - [x] [Deployment details page is not mobile friendly](https://gitlab.com/gitlab-org/gitlab/-/issues/458785)
      - [x] [Modal for adding new deployment rules does not warn users if rule already exists](https://gitlab.com/gitlab-org/gitlab/-/issues/438417)
      - [x] [Create empty state when Protected Environment has no approval rules](https://gitlab.com/gitlab-org/gitlab/-/issues/393402)
