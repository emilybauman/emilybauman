## Week of December 9th, 2024 to December 13th, 2024

## 17.7 Priorities
- Duo Chat
- Amazon Q
- Beautifying our UI

### Key Meetings
 - [x] Andrew & Emily Monthly ☕
 - [x] Emily / Pini catch up
 - [x] Emily & Rayana ☕
 - [x] Torsten / Emily - Duo Chat
 - [x] [REC] UX Call
 - [x] Emily / Taylor Coffee Chat & Learn About Duo in VSCode
 - [x] Emily / Libor (design pair)
 - [x] ☕ Timo & Emily
 - [x] Emily / Jacki 
 
### Tasks for Duo Chat:
 - [x] Get caught up to speed on the following Issues/Epics
      - [x] [DRAFT: Iteration 2: Multi-threaded conversations - List & CRUD (in IDE & GitLab Web Page)](https://gitlab.com/groups/gitlab-org/-/epics/16108).
      - [x] [Reconcile `/clear`, `/clean`, `/reset` slash commands in Duo Chat](https://gitlab.com/gitlab-org/gitlab/-/issues/470818).
      - [x] [Multi-threaded conversations in the chat (IDE/in-product)](https://gitlab.com/groups/gitlab-org/-/epics/15551).
 - [x] Start working on design for [DRAFT: Iteration 2: Multi-threaded conversations - List & CRUD (in IDE & GitLab Web Page)](https://gitlab.com/groups/gitlab-org/-/epics/16108).

### Tasks for Q: 
 - [x] Support Patrick with the upcoming Research. 
     - [x] Review Research Plan. 
 
### Other Tasks:
 - [ ] Complete more Beautifying our UI issues:
     - [x] [Re-evaluate Kubernetes agent registration dialog](https://gitlab.com/gitlab-org/gitlab/-/issues/506444).
     - [x] [Surface deployment information to the environment list view without using collabsibles](https://gitlab.com/gitlab-org/gitlab/-/issues/505770).
     - [ ] [Improve UX of registering a GitLab Agent](https://gitlab.com/gitlab-org/gitlab/-/issues/349809).
     - [ ] [Show the previous (successful) deployment job on the deployment details page](https://gitlab.com/gitlab-org/gitlab/-/issues/498077).
 - [ ] Shift Nudge Tasks
      - [ ] Finish Week 4 & 5 Homework. 
      - [x] Expense Course.
 - [x] Prepare for UX Showcase.
 - [x] Book Career Discussion Conversation with Jacki. 