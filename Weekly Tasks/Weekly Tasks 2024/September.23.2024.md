## Week of September 23rd, 2024 to September 27th, 2024

🌴 Slightly Short Week - OOO Monday for PTO

## 17.5 Priorities
- AI Settings 
- Duo Trials
- Q

### Key Meetings
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Michael / Emily Sync on Duo Code Review
 - [x] Patrick / Emily Design Sync
 - [x] Emily / Jay ☕️
 - [x] Jackie / Emily ☕
 - [x] Emily / Libor (design pair)
 - [x] Emily | Tiger Coffee Chat
 - [x] AI Framework - UX & FE Sync
 - [x] Emily/Veethika ☕️
 - [x] Growth Team Meeting
 - [x] Support <> AI Framework Sync
 - [x] Anna & Emily Coffee Chat
 - [x] Emily / Jacki
 - [x] Gina & Emily ☕

### Tasks for AI Settings:
 - [x] Support engineering with [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
      - [ ] Banner Design for [Remove Duo Settings from Admin Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493456). 
      - [ ] Banner Design for [Remove Duo Settings from Group Settings Page](https://gitlab.com/gitlab-org/gitlab/-/issues/493457).

### Tasks for Growth:
 - [x] Add content from doc into designs for [Trial Discover Page support all Duo Trials](https://gitlab.com/groups/gitlab-org/-/epics/15204).
 - [x] Continue work on [Add 'toggle' to Step 6 of Trial Registration flow to allow for invited users to be assigned a Duo Seat](https://gitlab.com/gitlab-org/gitlab/-/issues/480524).

### Tasks for Q:
- [x] Finish [UX: Assign issue to Q to create a task and open an MR](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/17).
- [x] Take a look at [UX: Upgrade a Java project](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/18).
- [x] Finish design for [UX GitLab Duo Settings for AWS/Amazon Q Bundle](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/32).

### Other Tasks:
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).
 - [ ] Shift Nudge Tasks
      - [ ] Finish watching Week 5 Material. 
      - [ ] Finish Week 5 Homework. 
 - [ ] Summzarize Learnings from Open Source Summit. 
 - [x] Expense Conference 
 - [x] Digital Accessibility Course 