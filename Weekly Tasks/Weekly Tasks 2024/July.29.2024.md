## Week of July 29th, 2024 to August 2nd, 2024

🌴 Slightly Short Week - Friday for Vacation.

## 17.3 Priorities
- AI Settings and Duo Trials

### Key Meetings
 - [x] Coffee chat - Emily + Russell
 - [x] Emily | Tiger Coffee Chat
 - [x] Emily | Taka Coffee Chat
 - [x] Mikhail | Emily Coffee Chat
 - [x] Rayana & Emily 1:1
 - [x] AI Framework - UX & FE Sync
 - [x] Nicolle & Emily ☕
 - [x] Emily / Austin / Tim <> AI Design Pair
 - [x] Sync on context research
 - [x] [Americas] CI/CD UX Meeting
 - [x] Anna & Emily Coffee Chat
 - [x] Emily & Gayle - UX / PM Sync
 - [x] Emily / Viktor weekly
 - [x] Emily / Marcel skip level
 - [x] Emily / Pini catch up

### Tasks for AI Settings:
 - [x] Continue work on [Duo settings move to new Duo page](https://gitlab.com/gitlab-org/gitlab/-/issues/472637).
 - [x] Finalize flow and get started on illustration for [[UX] UX and copy requirements for combined Ultimate + Duo Enterprise trial registration](https://gitlab.com/gitlab-org/gitlab/-/issues/468027/).
 - [x] Review [Duo: AI Context Management](https://gitlab.com/groups/gitlab-org/-/epics/14372).
 - [x] Review next Growth Issues. 

### Other Tasks:
 - [x] Review Environments and Import and Integrate design needs if capacity allows. 
    - [x] [List last events at the bottom of Kubernetes resource details](https://gitlab.com/gitlab-org/gitlab/-/issues/470041).
 - [ ] Continue working on [FY25 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/104).