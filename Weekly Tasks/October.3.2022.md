## Week of October 3rd, 2022 to October 7th, 2022

🌴 Short week - Family and Friends Day on Friday the 7th.

## 15.5 Priorities
- [15.5 Planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/issues/158)

### Key Meetings:
 - [x] Release Group Weekly [REC]
 - [x] Sunjung/Emily Coffee Chat
 - [x] Coffee Chat ☕
 - [x] UX Weekly Call
 - [x] UX Buddy session: Emily & Gina
 - [x] Release UX/FE sync
 - [x] CI/CD UX Meeting (EMEA/Americas)
 - [x] Emily / Jay ☕️
 - [ ] Rayana & Emily 1:1
 - [ ] Emily / Kevin - Sync
 - [ ] Deployment experience with Andrei
 - [ ] Michael / Emily - Design Pair
 
### Tasks for Release:
 - [x] Finish up [Improve Empty State Pages for Deployments](https://gitlab.com/gitlab-org/gitlab/-/issues/371392).
 - [ ] Finish prototype and usertesting.com guide for [Solution Validation: Validate users are okay being notified of Pending Deployment Approvals with To-Dos](https://gitlab.com/gitlab-org/ux-research/-/issues/2070).
 - [x] Finish up [Don't automatically run a deployment job post approval when using Unified Approval Setting](https://gitlab.com/gitlab-org/gitlab/-/issues/367943).
 - [ ] Get a first iteration done for [Add dynamically populated organization-level environments page](https://gitlab.com/gitlab-org/gitlab/-/issues/241506).
 - [ ] Finish research proposal for [CMS Scorecard - Release: FY22-Q3-Q4 - Environment Management Viable](https://gitlab.com/gitlab-org/gitlab/-/issues/341526).
 - [ ] Get a first iteration done for [Provide a mechanism to clean up stale environments](https://gitlab.com/gitlab-org/gitlab/-/issues/19724).

### Other Tasks:
 - [ ] Tend to feedback on the [Login Page Redesign](https://gitlab.com/gitlab-org/gitlab/-/issues/372425).
 - [ ] If time permits, help Eugie with the [Arkose Proposal](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/96). 

 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
