## Week of September 20th to September 24th, 2021

## 14.4 Priorities
 - [Expansion Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/439)
 - [Activation Priorities](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/437)

### Key Meetings:
 - [x] Coffee Chat with Veethika
 - [x] 1:1 with Jacki
 - [x] Sync with Jensen
 - [x] Jacki's UX Team Meeting
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] Growth Engineering Weekly
 - [x] 1:1 with Matej
 - [x] Aquisitions Team Sync
 - [x] UX Showcase
 - [x] Confetti Chat with Jackie
 - [ ] Andy and Emily Design Pair
 
### Tasks for Activation:
 - [x] Finish up content work for [Pitch a paid plan when users click to purchase CI minutes](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/420).
 - [ ] Work with Jensen on next steps for [Project Orchestration idea](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/418).
 - [x] Plan any additional work for 14.4

### Tasks for Expansion:
 - [ ] Work with Nick on the synthesis of [Allow non-admins to request access to a feature/tier or recommend an invite](https://gitlab.com/gitlab-org/ux-research/-/issues/1297).
 - [ ] Finish up Recruitment issue for [Problem validation: Invited user onboarding](https://gitlab.com/gitlab-org/ux-research/-/issues/1009).
 - [x] Work with Jackie Fraser on [Display invite modal post signup and group/project creation utilizing peak end rules](https://gitlab.com/gitlab-org/gitlab/-/issues/336248).
 - [ ] Take a look at new research work around [Problem Validation: How are non-invited users finding their team's namespace + groups/projects when they create new accounts?](https://gitlab.com/gitlab-org/gitlab/-/issues/340755).
 - [ ] Finalize content changes for [Experiment - Add expiration copy to invited user email](https://gitlab.com/gitlab-org/gitlab/-/issues/337860).
 - [x] Get appropriate data to continue work on [Experiment - Notify non-admin users to contact admin to invite new users to namespace](https://gitlab.com/gitlab-org/gitlab/-/issues/337555)


### Other Tasks:
 - [ ] Ongoing work around [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [ ] Make [Growth L&D Recommendations](https://gitlab.com/gitlab-org/growth/ui-ux/-/issues/102) reccomendations when I find things. 

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
 - [ ] Urdu Lessons
 
### Issue Backlog:

**To Be Done**

**Designs complete, waiting on implementation**
- [Provide admins with a sharable invite signup URL issue](https://gitlab.com/gitlab-org/gitlab/-/issues/336240/)
- [Add "welcome" questions to namespace creation UX](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/392).
- [Switch Easy-Button Icons with Radio Buttons](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/400).
- [Link CI Onboarding MR Widget to the Pipeline Editor](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/399).
- [Ask admin (inviter) what areas they'd like the invitee to focus on](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Notify inviter if invite email hard bounces](https://gitlab.com/gitlab-org/gitlab/-/issues/297543).
- [Net Improvement - Fix Invite modal copy to accurately communicate "access expires" impact](https://gitlab.com/gitlab-org/gitlab/-/issues/337865). 
- [Add guided walkthrough to pipeline editor](https://gitlab.com/gitlab-org/gitlab/-/issues/334659)
