## Week of March 3rd, 2025 to March 7th, 2025

## 17.10 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] Duo Chat Weekly
 - [x] Emily / Leala
 - [x] [REC] AI-Powered Monthly
 - [x] Discuss Duo with Q Integration Settings
 - [x] Jesse / Emily Onboarding Buddy
 - [x] Emily / Lina Design Pair
 - [x] Interview
 - [x] AI UX Share (AMER) 
 - [x] Emily / Nico Coffee Chat
 - [x] Torsten / Emily Sync
 - [x] Anna & Emily Coffee Chat
 - [x] #ai-virtual-coffee Donut
 - [x] ☕ Hunter & Emily
 - [x] Emily / Jacki
 - [x] Customer Interview 1
 - [x] Customer Interview 2
 - [x] Customer Interview 3

  
### Tasks for Duo Chat:
 - [x] Conduct interviews for [Solution Validation: Find-ability of Resizable Mechanism for Duo Chat Container](https://gitlab.com/gitlab-org/ux-research/-/issues/3344).
 - [x] Support team with final questions for [Composite Identity onboarding and offboarding flow](https://gitlab.com/gitlab-org/gitlab/-/issues/514388).
 - [x] Support additional issues related to [Adding context to Duo Chat - UX Proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/520305).
     - [x] [UX Duo Chat - Pasting Large Amounts of Text](https://gitlab.com/gitlab-org/gitlab/-/issues/521745).
     - [x] [UX Duo Chat - Add Local Files to Chat](https://gitlab.com/gitlab-org/gitlab/-/issues/521752).  
     - [x] [UX Duo Chat - Provide a Preview Window in Chat](https://gitlab.com/gitlab-org/gitlab/-/issues/521758).
     - [ ] [Duo Chat - List Current Context in Chat](https://gitlab.com/gitlab-org/gitlab/-/issues/522022). 
 - [x] Test Multi-Threaded Chat/Chat History and provide feedback [here](https://gitlab.com/gitlab-org/gitlab/-/issues/522957). 
 - [x] Review [Design Sprint plan](https://gitlab.com/gitlab-com/gitlab-ux/ai-powered-ux/emilyb-development/-/issues/3) with Torsten.
 - [ ] Review Duo Chat Roadmap with Torsten. 

### Tasks for Q: 
 - [ ] Support Patrick with UX Reviews. 
 - [ ] Support Health Check Reviews. 
 
### Other Tasks:
 - [ ] Start on Design Sprint Planning. 