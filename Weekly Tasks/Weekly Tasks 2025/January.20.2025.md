## Week of January 20th, 2025 to January 24th, 2025

## 17.9 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] Andrei & Emily Monthly ☕
 - [x] Andrew & Emily Monthly ☕
 - [x] Lindsey / Emily Coffee Chat
 - [x] Jackie / Emily Sync
 - [x] Emily / Libor Design Pair
 - [x] #ai-virtual-coffee Donut
 - [x] Emily / Sunjung Quarterly Coffee Chat
 - [x] Emily / Jacki
 - [x] #ai-virtual-coffee Donut
  
### Tasks for Duo Chat:
 - [x] Finalize research issue for Resizable Mechanism for Duo Chat Container.
 - [x] Continue to help engineering on refinement for [[UX] Iteration 2: Multi Threaded Conversations (GitLab Web Page)](https://gitlab.com/gitlab-org/gitlab/-/issues/509168/).
 - [ ] Take a look at [Duo Chat in Web: Explain code feature prints `/explain` into the chat giving the wrong impression that this could be used by the user](https://gitlab.com/gitlab-org/gitlab/-/issues/461270).
 - [x] Support [Dark mode > Duo Chat group](https://gitlab.com/groups/gitlab-org/-/epics/16512).

### Tasks for Q: 
 - [x] Follow conversations for needs for [P2 - Usage and Consumption visibility](https://gitlab.com/groups/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/-/epics/14).
 - [ ] Finish design for [Add button for /fix quick action](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/365).
 - [ ] Start on [Add UI disclaimer for AI generated in Code in merge requests](https://gitlab.com/gitlab-org/gitlab/-/issues/514790).
 
### Other Tasks:
 - [ ] Close off and share outcomes of [✨ Beautifying our UI (17.7-17.8) ✨](https://gitlab.com/gitlab-org/gitlab/-/issues/506374) and start working on [[Draft] Blog: ✨ Beautifying our UI (17.7-17.8) ✨](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/content-strategy-and-ops/blog/-/issues/293). 
 - [ ] Finish Self-Review.
 - [x] Finish [FY26 - Emily Bauman - Individual Growth Plan](https://gitlab.com/gitlab-com/gitlab-ux/ai-powered-ux/emilyb-development/-/issues/1).