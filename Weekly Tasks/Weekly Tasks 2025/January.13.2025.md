## Week of January 13th, 2025 to January 17th, 2025

- Slightly short week, at the UX Social Meetup Monday and Tuesday. 

## 17.9 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] Emily / Pini catch up
 - [x] Support <> AI Framework Sync
 - [x] Anna & Emily Coffee Chat
 - [x] Data Science section UX meetup
 - [x] Emily / Jacki
 - [x] Patrick / Emily Design Sync

### Tasks for Duo Chat:
 - [ ] Open and finalize research issue for Resizable Mechanism for Duo Chat Container.
 - [x] Continue to help engineering on refinement for [[UX] Iteration 2: Multi Threaded Conversations (GitLab Web Page)](https://gitlab.com/gitlab-org/gitlab/-/issues/509168/).

### Tasks for Q: 
 - [x] Review needs for [P2 - Usage and Consumption visibility](https://gitlab.com/groups/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/-/epics/14).
 
### Other Tasks:
 - [ ] Close off and share outcomes of [✨ Beautifying our UI (17.7-17.8) ✨](https://gitlab.com/gitlab-org/gitlab/-/issues/506374). 
 - [x] Expense items from NYC UX Social Meetup. 
 - [x] Expense internet.
 - [x] Open and fill out IGP Issue. 
 - [ ] Start on Self-Review.