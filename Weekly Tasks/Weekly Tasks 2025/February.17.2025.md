## Week of February 17th, 2025 to February 121st, 2025

🌴 Slightly Short Week - OOO Monday

## 17.9 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] Andrei & Emily Monthly ☕
 - [x] AI UX Weekly Share (EMEA)
 - [x] Patrick / Emily Design Sync
 - [x] ☕ Danika/Emily
 - [x] Emily / Lina Design Pair
 - [x] ☕ Timo & Emily
 - [x] Lindsey / Emily Coffee Chat
 - [x] Emily / Jacki
  
### Tasks for Duo Chat:
 - [ ] Finalize Sandbox and Start Recruitment for [Solution Validation: Find-ability of Resizable Mechanism for Duo Chat Container](https://gitlab.com/gitlab-org/ux-research/-/issues/3344).
 - [ ] Finalize [Composite Identity onboarding and offboarding flow](https://gitlab.com/gitlab-org/gitlab/-/issues/514388).
 - [x] Support [Iteration 2: Multi-threaded conversations - List & CRUD (GitLab Web Page)](https://gitlab.com/groups/gitlab-org/-/epics/16108).
 - [x] Start looking into [Planning: Chat with your code base initiative](https://gitlab.com/gitlab-org/gitlab/-/issues/517381).

### Tasks for Q: 
 - [x] Finish [Add link to AWS docs for steps to confirm Q usage from GitLab Settings](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/428).
 - [x] Support Patrick with UX Reviews. 
 
### Other Tasks:
 - [ ] Finalize [[Draft] Blog: ✨ Beautifying our UI (17.7-17.8) ✨](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/content-strategy-and-ops/blog/-/issues/293). 
 - [ ] Start on Design Sprint Planning. 
