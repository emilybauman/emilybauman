## Week of January 6th, 2025 to January 10th, 2025

## 17.8 Priorities
- Duo Chat - Multi-Threaded Conversations
- Amazon Q
- Beautifying our UI

### Key Meetings
 - [x] Valerie / Emily
 - [x] Patrick / Emily Design Sync
 - [x] Emily / Lindsey FE/UX Settings Bug Sync
 - [x] UX Forum
 - [x] Jacki / Emily Sync & IGP Discussion
 - [x] Emily / Jackie
 
### Tasks for Duo Chat:
 - [x] Finalize and launch research plan for [[UX] Iteration 2: Multi Threaded Conversations (GitLab Web Page)](https://gitlab.com/gitlab-org/gitlab/-/issues/509168/).
 - [x] ~~Open IDE Version and Finalize Design.~~

### Tasks for Q: 
 - [x] Review what work is planned next. 
 
### Other Tasks:
 - [ ] Complete more Beautifying our UI issues:
     - [ ] [Show the previous (successful) deployment job on the deployment details page](https://gitlab.com/gitlab-org/gitlab/-/issues/498077).
     - [ ] [Add call-to-action on Project settings > CI/CD page for features that are only available with an upgraded license](https://gitlab.com/gitlab-org/gitlab/-/issues/389451).
- [x] Review MRs
- [x] Finish Interview and Scorecard.