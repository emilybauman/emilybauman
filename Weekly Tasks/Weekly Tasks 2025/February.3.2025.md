## Week of February 3rd, 2025 to February 7th, 2025

🌴 Slightly Short Week - OOO Friday

## 17.9 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] UX All Hands
 - [x] FW: health check api ui
 - [x] Emily / Libor (design pair)
 - [x] Emily & Rayana ☕
 - [x] AI UX Share (AMER)
 - [x] Anna & Emily Coffee Chat
 - [x] #ai-virtual-coffee Donut
 - [x] Patrick / Emily Design Sync
 - [x] ☕ Hunter & Emily
 - [x] Emily / Jacki
 - [x] Emily / Jacki - Review IGP Q1
  
### Tasks for Duo Chat:
 - [ ] Prepare UX Sandbox and Start recruiting for [Solution Validation: Find-ability of Resizable Mechanism for Duo Chat Container](https://gitlab.com/gitlab-org/ux-research/-/issues/3344).
 - [x] Support [Dark mode > Duo Chat group](https://gitlab.com/groups/gitlab-org/-/epics/16512).

### Tasks for Q: 
 - [x] Finish [Add button for /fix quick action](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/365).
 - [ ] Finish [Add UI disclaimer for AI generated in Code in merge requests](https://gitlab.com/gitlab-org/gitlab/-/issues/514790).
 
### Other Tasks:
 - [ ] Finish [[Draft] Blog: ✨ Beautifying our UI (17.7-17.8) ✨](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/content-strategy-and-ops/blog/-/issues/293). 
