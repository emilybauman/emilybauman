## Week of February 24th, 2025 to February 28th, 2025

## 17.10 Priorities
- Duo Chat 
- Amazon Q

### Key Meetings
 - [x] FY26 Company Kickoff (CKO) : Option 1
 - [x] PD (Jacki's Directs) Skip Level with Angela
 - [x] Emily / Lina Design Pair
 - [x] Emily | Tiger Coffee Chat
 - [x] #ai-virtual-coffee Donut
 - [x] #ai-virtual-coffee Donut
 - [x] FY26 SKO (Virtual) - AMER
 - [x] Emily / Pini catch up
 - [x] Emily / Viktor - Coffee Chat
 - [x] Torsten / Emily Sync
 - [x] Data Science section UX meetup
 - [x] Emily / Jacki
 - [x] Emily / Jay ☕️
  
### Tasks for Duo Chat:
 - [x] Finalize Sandbox and Start Recruitment for [Solution Validation: Find-ability of Resizable Mechanism for Duo Chat Container](https://gitlab.com/gitlab-org/ux-research/-/issues/3344).
 - [x] Finalize [Composite Identity onboarding and offboarding flow](https://gitlab.com/gitlab-org/gitlab/-/issues/514388).
 - [x] Support [Adding context to Duo Chat - UX Proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/520305).
 - [x] Start looking into [Planning: Chat with your code base initiative](https://gitlab.com/gitlab-org/gitlab/-/issues/517381).

### Tasks for Q: 
 - [x] Finish [Add link to AWS docs for steps to confirm Q usage from GitLab Settings](https://gitlab.com/gitlab-com/ops-sub-department/aws-gitlab-ai-integration/integration-motion-planning/-/issues/428).
 - [ ] Support Patrick with UX Reviews. 
 - [x] Support Health Check Reviews. 
 
### Other Tasks:
 - [x] Finalize [[Draft] Blog: ✨ Beautifying our UI (17.7-17.8) ✨](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/content-strategy-and-ops/blog/-/issues/293). 
 - [ ] Start on Design Sprint Planning. 