## Week of May 2nd to May 6th, 2022

🌴 - OOO Thursday May 5th and Friday May 6th

## 15.0 Priorities
- Activation/Adoption Planning Issue: None for this Milestone 
- [Expansion/Conversion Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/629)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/628)

### Key Meetings:
 - [x] Coffee chat with Veethika
 - [x] UX Weekly Call
 - [x] Growth Conversion Team Meeting
 - [x] 1:1 Emily B and Jacki
 - [x] Coffee chat with Hinam
 - [x] Activation Team weekly sync - APAC

### Tasks for Activation:
 - [ ] Finalize content on [Email code verification when users meet high risk criteria](https://gitlab.com/gitlab-org/modelops/anti-abuse/pipeline-validation-service/-/issues/100)

### Tasks for Expansion:
 - [x] Finalize content on [UX: alert banner group usage quota seats page](https://gitlab.com/gitlab-org/gitlab/-/issues/360171).
 - [x] Finalize content on [UX: Resolve remaining Awaiting user UI discrepancies on Group/Projects and Your Groups pages.
](https://gitlab.com/gitlab-org/gitlab/-/issues/356654)
 - [x] Take a look at [UX: Add premium trial CTA to revamped /billings page](https://gitlab.com/gitlab-org/gitlab/-/issues/357582).

### Other Tasks:
 - [ ] Continue coverage of Arkose work.

### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
