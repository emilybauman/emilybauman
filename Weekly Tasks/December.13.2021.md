## Week of December 13th, 2021 to December 17th, 2021

## 14.6 Priorities
- [Activation Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/504)
- Expansion Planning Issue — Combined with [Conversion's planning issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/505) this milestone.

### Key Meetings:
 - [x] 1:1 with Jacki
 - [x] Jacki's Team Meeting
 - [x] Growth Weekly
 - [x] UX Weekly
 - [x] Growth Engineering Weekly
 - [ ] 1:1 with Matej
 - [x] Aquisitions Team Sync
 - [x] Growth Conversion Team Meeting
 - [x] Async with Kevin
 - [x] Design Pair with Andy

### Tasks for Activation:
 - [x] Finish first iteration for Require CC for namespaces creation
 - [x] Finish up work on [Add confirm identity via phone number to validation in first mile](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/501)
 - [ ] Start work on [Add "setup a self-hosted runner" to the pipeline zerostate page](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/465)

### Tasks for Expansion:
 - [x] Finish up work on [Add high conversion trial actions to Continuous onboarding](https://gitlab.com/gitlab-org/gitlab/-/issues/345174)
 - [ ] Finish first iteration of design for [Experiment UX: during cart abandonment offer trial and talk to sales option
](https://gitlab.com/gitlab-org/gitlab/-/issues/343179)
- [x] Review discussion guide for [Billing/Purchase Page Discoverability Unmoderated Test](https://docs.google.com/document/d/1EBfO84Jdta6KGdwx_SQ-rQLlevNcWmvbuSkg4tIN4XI/edit?usp=sharing)

### Other Tasks:
 - [ ] Restart work on [New User Onboarding for the Explorer](https://gitlab.com/gitlab-org/gitlab/-/issues/337258).
 - [x] Finish heuristic buddy tasks on [Manage:Optimize FY22-Q4 - Evaluating VSA](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12593)

### Personal Goals:
 - [ ] Continue working away at [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201).
