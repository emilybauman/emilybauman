## Week of May 23rd, 2022 to May 27th, 2022

- 🌴 Short week with Victoria Day (Public Holiday) on the 23rd and Family & Friend's Day on the 27th

## 15.1 Priorities
- Activation/Adoption Planning Issue: None for this Milestone 
- [Expansion/Conversion Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/4152983?label_name%5B%5D=group%3A%3Aconversion&milestone_title=15.1)
- [UX Planning Issue](https://gitlab.com/gitlab-org/growth/team-tasks/-/issues/637)

### Key Meetings:
 - [x] Jacki's UX Team (FG) Meetup [REC]
 - [x] UX Weekly Call
 - [x] UX Showcase
 - [x] Emily / Taurie - Catch up/Coffee Chat
 - [x] Emily / Gayle/Sam Bi-Weekly
 - [x] 1:1 Emily B and Jacki
 - [x] Jensen/Emily Sync
 - [x] Interview with Product Manager & Engineering Manager

### Tasks for Activation:
 - [ ] Take a look at [Determine barriers to entry at signup (from Arkose risk score)](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/9).
 - [x] Answer questions for [An admin has an allowlist for the set git_upload_pack settings](https://gitlab.com/gitlab-org/modelops/anti-abuse/team-tasks/-/issues/17).

### Tasks for Expansion:
 - [x] Take a look at [UX: Trial wind down: in-product messaging and email ahead of trial end for namespaces over 5 user limit
](https://gitlab.com/gitlab-org/gitlab/-/issues/347441).

### Other Tasks:
 - [x] Keep adding ideas to [Beautifying Our UI 15.2](https://gitlab.com/gitlab-org/gitlab/-/issues/362122)
 
### Personal Goals:
 - [ ] Finish up new [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and add them to the [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915).
