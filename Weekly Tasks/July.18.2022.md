## Week of July 18th to July 22nd, 2022

## 15.3 Priorities
- [UX Transition Plan for Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1998)
- [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999)

### Key Meetings:
 - [x] Rayana /Jacki/Emily Mid Year Check-In
 - [x] Monthly Release Kickoff (Public Livestream)
 - [x] Emily / Katie chat release + package
 - [x] Release Group Weekly [REC]
 - [x] Release, Configure, Monitor Field Sync
 - [x] UX Buddy session: Emily & Gina
 - [x] UX Weekly Call
 - [x] Coffee chat
 - [x] Release UX/PM sync
 - [x] Release UX/FE sync
 - [x] UX Showcase
 - [x] Emily / Kevin - Sync
 - [x] 1:1 Emily B and Jacki
 
### Tasks for Release:
 - [x] Continue onboarding tasks in [Product Designer Onboarding: Emily Bauman, as Product Designer - Release](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Finish up first iterations of JTBDs for [Task 2: Prepare 1-2 JTBDs for the Release Orchestration (RO) category.](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1999).
 - [x] Pick up an issue or two from the [planning board](https://gitlab.com/groups/gitlab-org/-/boards/1489550?label_name%5B%5D=Next%20Up&label_name%5B%5D=devops%3A%3Arelease&label_name%5B%5D=group%3A%3Arelease). 

### Other Tasks:
 - [ ] Finish up work on [redesigning the login page](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/91673). 
 
### Personal Goals:
 - [ ] Continue to work on [year end goals](https://app.mural.co/invitation/mural/gitlab2474/1627485922391?sender=u1f7d2dc10830b351493b2706&key=d468f21f-b347-410d-951e-930015a16201) and [Goals issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1915) to reflect the new team.
